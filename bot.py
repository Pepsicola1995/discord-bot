import asyncio
import logging
import traceback

import aiohttp
import discord
from discord.ext import commands
from discord.ext import tasks

import config


initial_extensions = [
    'cogs.locale',
    'cogs.birthdays',
    'cogs.welcome',
    'cogs.steam',
    'cogs.twitch',
    'cogs.discord_streams',
    'cogs.reaction_roles',
    'cogs.custom_commands',
    'cogs.testing',
    'cogs.text_transformation',
    'cogs.remindme',
    'cogs.admin',
    'cogs.rtfm',
    'cogs.vps',
    'cogs.activity',
    'cogs.what',
    'cogs.stocks',
    'cogs.owo',
    'cogs.emoji',
    'cogs.jabomons',
    # 'cogs.ttt',
    'cogs.powerrankings',
]


def init_logger():
    """
    Initialize the logger

    Create a console logger and a seperate file logger
    Filter urllib and discord logs
    """
    log_formatter = logging.Formatter('%(asctime)s [%(name)-16.16s] '
                                      '[%(levelname)-4.4s]  %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
    urllib_filter = logging.Filter()
    urllib_filter.filter = (lambda record: 'urllib' not in record.name
                                           and 'discord' not in record.name)
    emote_filter = logging.Filter()
    emote_filter.filter = lambda record: 'fileOnly' not in record.name

    file_handler = logging.FileHandler('bot.log')
    file_handler.setFormatter(log_formatter)
    file_handler.addFilter(urllib_filter)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    console_handler.addFilter(urllib_filter)
    console_handler.addFilter(emote_filter)

    rootLogger = logging.getLogger()
    rootLogger.addHandler(file_handler)
    rootLogger.addHandler(console_handler)
    rootLogger.setLevel(logging.INFO)


def init_discord_logger():
    # TODO: more extensive configuration
    logger = logging.getLogger('discord')
    logger.setLevel(logging.INFO)


class Bot(commands.Bot):

    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        self.aiohttp_session = None
        self.config: config.Config = config.config
        super().__init__(*args, **kwargs)

    async def setup_hook(self):
        self.aiohttp_session = aiohttp.ClientSession(loop=asyncio.get_running_loop())
        for extension in initial_extensions:
            try:
                await self.load_extension(extension)
                self.logger.info('Successfully loaded %s', extension)
            except Exception:
                self.logger.error('Failed to load initial extension %s: %s',
                                  extension,
                                  traceback.format_exc())
        self.join_active_threads.start()

    async def on_message(self, _message: discord.Message):
        """
        Emptied so the commands wont be executed twice, all command execution
        happens in cogs/custom_commands.py
        """
        pass

    async def on_ready(self):
        if self.user is not None:
            self.logger.info('Successfully logged in as %s - %s',
                             self.user.name,
                             self.user.id)
        self.logger.info('discordpy version: %s.', discord.__version__)
        self.logger.info('Ready')

    async def on_thread_create(self, thread: discord.Thread):
        if self.user is None:
            return
        try:
            await thread.fetch_member(self.user.id)
        except discord.NotFound:
            self.logger.info('Detected newly created thread %s - attempting to join!',
                             thread.name)
            await thread.join()
        except Exception:
            return

    @tasks.loop(hours=12)
    async def join_active_threads(self):
        if self.user is None:
            return
        for guild in self.guilds:
            for thread in await guild.active_threads():
                try:
                    await thread.fetch_member(self.user.id)
                except discord.NotFound:
                    self.logger.info('Found unjoined thread %s - joining!', thread.name)
                    await thread.join()
                except Exception:
                    continue

    @join_active_threads.before_loop
    async def before_join_active_threads(self):
        await self.wait_until_ready()


if __name__ == '__main__':
    init_logger()
    bot = Bot(command_prefix='!',
              activity=discord.Game(name='!help <command>', type=1),
              intents=discord.Intents.all(),
              member_cache_flags=discord.MemberCacheFlags.all())
    bot.run(config.config.token, reconnect=True)
