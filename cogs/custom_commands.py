import logging
import re
from typing import Optional

import discord
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.commands import Base
from db.commands import Command


class CustomCommandsCogs(commands.Cog):
    """
    Cog for custom commands

    Allows users with one of the permitted_roles to
    create AND delete custom commands. The bot does
    not keep track of who created a command, so anyone
    with the role(s) can potentially delete anyones
    commands.

    A custom commands is just a simple text response to a
    message starting with '!'.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.permitted_roles = [self.bot.config.ids.knight_role_id]  # list of role ids
        self.excluded_users = [self.bot.config.ids.no_command_user_id]

    async def cog_unload(self):
        self.logger.debug('Closing db session...')

    # check
    def has_permissions(self, author):
        """
        Check whether the author has permission to interact with custom
        commands

        Parameters
        ----------
        author : discord.Member()
            the author of the message
        """
        if set(self.permitted_roles).intersection({role.id for role in author.roles}):
            return author.id not in self.excluded_users
        return False

    async def add_command(self, session: AsyncSession, name: str, content: str) -> None:
        cmd = Command(name=name, content=content)
        async with session.begin():
            session.add(cmd)

    async def get_command(self, session: AsyncSession, name: str) -> Optional[Command]:
        stmt = select(Command).where(Command.name == name)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def del_command(self, session: AsyncSession, name: str) -> None:
        cmd = await self.get_command(session, name)
        async with session.begin():
            await session.delete(cmd)

    async def edit_command(self, session: AsyncSession, name: str, content: str) -> None:
        cmd = await self.get_command(session, name)
        async with session.begin():
            cmd.content = content
            session.add(cmd)

    async def get_commands_containing(self, session: AsyncSession, text: str) -> list[Command]:
        stmt = select(Command).where(Command.name.contains(text))
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_all_commands(self, session: AsyncSession) -> list[Command]:
        async with session.begin():
            return (await session.execute(select(Command))).scalars().all()

    @commands.group(name="commands", aliases=["command"])
    async def custom_commands(self, ctx):
        """
        Group for command commands (xD)
        Can only be used by knights except scheist.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @custom_commands.command(name="add")
    async def commands_add(self, ctx, comm=None, *, text=None):
        """
        Add a custom command.

        Parameters
        ----------
        comm : str
            the name of the command you want to add
        text : str
            the text the command should return

        Example
        -------
        >>> !commands add dexzy @dexzy
        Creates a command named "dexzy" which returns the text "@dexzy".
        That command can be used by typing "!dexzy"
        """
        async with db.async_sessionmaker() as session:
            if comm is None or text is None:
                return await ctx.send_help(ctx.command)
            if not self.has_permissions(ctx.author):
                return await ctx.send("Only proper Knights are allowed to add commands.")
            if not re.match(r"^[A-Za-z0-9]+[A-Za-z0-9_]*[A-Za-z0-9]+$", comm):
                return await ctx.send("You're only allowed to use **letters**, "
                                      "**numbers** and **underscores** in your "
                                      "command.\nYour command can not start or "
                                      "end with an underscore.")
            if len(comm) > 32:
                return await ctx.send("The maximum command length is "
                                      "**32 characters**.")
            if len(text) > 1200:
                return await ctx.send("The maximum text length for custom "
                                      "commands is **1200 characters**.")
            if (await self.get_command(session, comm)) is not None:
                return await ctx.send(f"The command `{comm}` already exists.")
            if comm in list(map(lambda x: x.name, self.bot.commands)):
                return await ctx.send("You can't overwrite the hardcoded command "
                                      f"`{comm}`.")
            await self.add_command(session, comm, text)
        self.logger.info("Command '%s' added to server %s - %d by %s - %d",
                         comm, ctx.guild.name, ctx.guild.id, ctx.author, ctx.author.id)
        return await ctx.send(f"Successfully added the command `{comm}`.")

    @custom_commands.command(name="remove", aliases=["delete", "del"])
    async def commands_remove(self, ctx, comm=None):
        """
        Remove a custom command.

        Parameters
        ----------
        comm : str
            the name of the command you want to remove

        Example
        -------
        >>> !commands remove dexzy
        Removes the custom command named "dexzy"
        """
        if comm is None:
            return await ctx.send_help(ctx.command)
        if not self.has_permissions(ctx.author):
            return await ctx.send("Only proper Knights are allowed to remove "
                                  "commands.")
        async with db.async_sessionmaker() as session:
            if (await self.get_command(session, comm)) is None:
                await ctx.send_help(ctx.command)
                return await ctx.send(f"Couldn't find the custom command `{comm}`.")
            await self.del_command(session, comm)
        self.logger.info("Command '%s' removed from server %s - %d by %s - %d",
                         comm, ctx.guild.name, ctx.guild.id, ctx.author, ctx.author.id)
        return await ctx.send(f"Successfully deleted the command `{comm}`.")

    @custom_commands.command(name="edit")
    async def commands_edit(self, ctx, comm=None, *, text=None):
        """
        Edit an existing custom command
        Parameters
        ----------
        comm : str
            the name of the command you want to edit
        text : str
            the new text that the command should return

        Example
        -------
        >>> !commands edit dexzy haHAA
        Makes the existing command "dexzy" return the text "haHAA"
        """
        async with db.async_sessionmaker() as session:
            if comm is None or text is None:
                return await ctx.send_help(ctx.command)
            if not self.has_permissions(ctx.author):
                return await ctx.send('Only proper Knights are allowed to add commands.')
            if (await self.get_command(session, comm)) is None:
                await ctx.send_help(ctx.command)
                return await ctx.send(f"Couln't find the custom command {comm}")
            if len(text) > 1200:
                await ctx.send_help(ctx.command)
                return await ctx.send("The maximum text length for a custom "
                                      "command is **1200 characters**.")
            await self.edit_command(session, comm, text)
        self.logger.info("Command '%s' edited in server %s - %d by %s - %d.",
                         comm, ctx.guild.name, ctx.guild.id, ctx.author, ctx.author.id)
        return await ctx.send(f"Successfully edited the command `{comm}`")

    @custom_commands.command(name="list")
    async def commands_list(self, ctx):
        """
        Show a list of all custom commands
        """
        async with db.async_sessionmaker() as session:
            all_custom_commands = await self.get_all_commands(session)
        if len(all_custom_commands) == 0:
            return await ctx.send("There are no custom commands yet.")
        answer = '```\n'
        answer += '\n'.join(c.name for c in all_custom_commands)
        answer += '```'
        return await ctx.send(answer)

    @custom_commands.command(name="search")
    async def commands_search(self, ctx, text=None):
        """
        Show a list of commands that contain some text

        Parameters
        ----------
        text : str
            the text that you want to search for

        Example
        -------
        >>> !commands search dex
        Returns a list of custom commands containing 'dex'
        """
        if text is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You have to specify what you're searching for.")
        async with db.async_sessionmaker() as session:
            matching_commands = await self.get_commands_containing(session, text)
        if len(matching_commands) == 0:
            return await ctx.send("There are no custom commands matching that query.")
        answer = "```\n"
        answer += '\n'.join(c.name for c in matching_commands)
        answer += "```"
        await ctx.send(answer)

    # aliases
    @commands.command(help=commands_add.help, hidden=True)
    async def addcom(self, ctx, comm=None, *, text=None):
        await ctx.invoke(self.commands_add, comm=comm, text=text)

    @commands.command(help=commands_remove.help, hidden=True)
    async def delcom(self, ctx, comm=None):
        await ctx.invoke(self.commands_remove, comm=comm)

    @commands.command(help=commands_edit.help, hidden=True)
    async def editcom(self, ctx, comm=None, *, text=None):
        await ctx.invoke(self.commands_edit, comm=comm, text=text)

    # internal
    async def process_commands(self, session: AsyncSession, message: discord.Message):
        command = await self.get_command(session, message.content[1:])
        if command is None:
            return
        channel = message.channel
        await channel.send(command.content)
        self.logger.info('%s - %d used the custom command %s in channel %s - %d',
                         message.author,
                         message.author.id,
                         message.content,
                         message.channel,
                         message.channel.id)

    async def check_valid_custom_command(self, session: AsyncSession, message: discord.Message):
        """
        Check whether a message was a valid custom command
        """
        if message.author.bot:
            return False
        if message.content is None:
            return False
        if len(message.content) < 2:
            return False
        if not message.content.startswith('!'):
            return False
        return (await self.get_command(session, message.content[1:])) is not None

    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Look for a custom command call in the message, and if none was
        received, the bots process_commands() routine will be called.
        """
        async with db.async_sessionmaker() as session:
            if await self.check_valid_custom_command(session, message):
                return await self.process_commands(session, message)
        ctx = await self.bot.get_context(message)
        if ctx.valid:
            await self.bot.process_commands(message)


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(CustomCommandsCogs(bot))
