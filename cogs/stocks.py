import aiohttp
import os.path
import logging
import time
from typing import Dict, Optional

import yaml
from discord.ext import commands, tasks
from tabulate import tabulate

from bot import Bot

PATH = "data/stocks/"
FILENAME = "portfolios.yml"
FILE_PATH = PATH + FILENAME

class RequestException(Exception):
    pass

class StocksCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.coin_base_url = 'https://api.coingecko.com/api/v3'
        self.coin_list = []
        self.price_cache = {}
        self.portfolios = {}
        self.load_data()

        self.update_coin_list_task.start()

    async def cog_unload(self):
        self.update_coin_list_task.cancel()

    async def make_request(self, endpoint: str, params: Dict[str, str]={}) -> dict:
        async with self.bot.aiohttp_session.get(self.coin_base_url + endpoint, params=params) as resp:
            resp: aiohttp.ClientResponse
            if resp.status >= 300:
                text = await resp.text()
                raise RequestException(f'Failed with error {resp.status}: {text}')
            json = await resp.json()
        return json

    def coin_exists(self, coin_id: str) -> bool:
        for coin_dct in self.coin_list:
            if coin_dct['id'] == coin_id:
                return True
        return False

    async def get_price(self, coin: str) -> float:
        if not coin in self.price_cache:
            self.price_cache[coin] = {'price': None, 'last_updated': -1}
        if time.time() - self.price_cache[coin]['last_updated'] > 60:
            self.price_cache[coin]['last_updated'] = time.time()
            self.price_cache[coin]['price'] = await self.fetch_price(coin)
        return self.price_cache[coin]['price']

    async def fetch_price(self, coin: str) -> float:
        endpoint = f'/coins/{coin}'
        params = {
            'localization': 'false',
            'tickers': 'false',
            'market_data': 'true',
            'community_data': 'false',
            'developer_data': 'false',
            'sparkline': 'false'
        }

        coin_data = await self.make_request(endpoint, params)
        return coin_data['market_data']['current_price']['usd']

    def ensure_portfolio(self, owner_id: int, coin: Optional[str]=None):
        if not owner_id in self.portfolios:
            self.portfolios[owner_id] = {'usd_invested': 0, 'usd_received': 0}
        if coin and not coin in self.portfolios[owner_id]:
            self.portfolios[owner_id][coin] = {'amount': 0, 'avg_buy_price': 0}

    def reset_portfolio(self, owner_id: int):
        self.portfolios[owner_id] = {'usd_invested': 0, 'usd_received': 0}

    def buy_coin(self, discord_id: int, coin_id: str, amount: float, price: float):
        self.ensure_portfolio(discord_id, coin_id)
        absolute_paid = self.portfolios[discord_id][coin_id]['amount'] * self.portfolios[discord_id][coin_id]['avg_buy_price']
        absolute_paid += price
        self.portfolios[discord_id][coin_id]['amount'] += amount
        self.portfolios[discord_id][coin_id]['avg_buy_price'] = absolute_paid / self.portfolios[discord_id][coin_id]['amount']
        self.portfolios[discord_id]['usd_invested'] += price
        self.save_data()

    def sell_coin(self, discord_id: int, coin_id: str, value: float, price: float):
        self.portfolios[discord_id][coin_id]['amount'] -= value
        self.portfolios[discord_id]['usd_received'] += price
        self.save_data()

    def get_portfolio_value(self, discord_id: int, coin_id: str):
        self.ensure_portfolio(discord_id, coin_id)
        return self.portfolios[discord_id][coin_id]

    @tasks.loop(hours=6)
    async def update_coin_list_task(self):
        endpoint = '/coins/list'
        try:
            coins = await self.make_request(endpoint)
        except RequestException as e:
            return
        self.coin_list = coins

    @commands.command()
    async def creset(self, ctx):
        """
        Reset your portfolio. This removes *all your portfolio activity*.
        """
        self.reset_portfolio(ctx.author.id)
        self.save_data()
        self.logger.info('%s just reset their stats', ctx.author)
        await ctx.send('Successfully reset your portfolio.')

    @commands.command()
    async def csearch(self, ctx, name):
        """
        Look up the ID of a coin

        Parameters
        ----------
        name : str
            part of the name you want to look up

        Example
        -------
        >>> !csearch bitcoin
        Shows you all the coin IDs of coins that have "bitcoin" in their name
        or description.
        """
        name = name.lower()
        matching_ids = []
        for coin in self.coin_list:
            coin_id = coin['id'].lower()
            coin_symbol = coin['symbol'].lower()
            coin_name = coin['name'].lower()
            if name in coin_id or name in coin_symbol or name in coin_name:
                matching_ids.append(coin_id)
        await ctx.send('```\n'+'\n'.join(matching_ids)+'\n```')

    @commands.command()
    async def cprice(self, ctx, coin_id):
        """
        Look up the price of a coin

        Parameters
        ----------
        coin_id : str
            The ID of the coin

        Example
        -------
        >>> !cprice dogecoin
        Shows you the current price of dogecoin
        """
        try:
            price = await self.get_price(coin_id)
        except RequestException as e:
            return await ctx.send(f'Something went wrong: {e}')

        await ctx.send(f'The current price for one {coin_id} is {price} USD.')

    @commands.command()
    async def fbuy(self, ctx, coin_id, usd, share_amount):
        """
        Buy some coin for a given value

        Parameters
        ----------
        coin_id : str
            The ID of the coin you want to buy

        usd : float
            The amount of USD you want to buy the coin for. Rounds to 2 digits.

        share_amount: float
            The amount of coin you want to buy for that price

        Example
        -------
        >>> !fbuy dogecoin 1 1000
        Buys you 1000 dogecoin for $1
        """
        try:
            buy_price = float(usd)
        except ValueError:
            return await ctx.send('Nice try, next time use a number for usd!')
        try:
            share_amount = float(share_amount)
        except ValueError:
            return await ctx.send('The amount of coin has to be a decimal number!')
        if buy_price < 0:
            return await ctx.send('I see what you did there ;) Don\'t buy for negative prices!')
        if share_amount < 0:
            return await ctx.send('I see what you did there ;) Don\'t buy negative amounts!')
        if not self.coin_exists(coin_id):
            return await ctx.send(f'I do not know the coin {coin_id}.')
        buy_price = round(buy_price, 2)
        self.buy_coin(ctx.author.id, coin_id, share_amount, buy_price)
        self.logger.info('%s just bought %f %s for %f usd', ctx.author, share_amount, coin_id, buy_price)
        await ctx.send(f'Successfully bought {share_amount} {coin_id} for {usd} USD.')

    @commands.command()
    async def cbuy(self, ctx, coin_id, usd):
        """
        Buy some coin

        Parameters
        ----------
        coin_id : str
            The ID of the coin you want to buy

        usd : float
            The amount of USD you want to buy the coin for. Rounds to 2 digits.

        Example
        -------
        >>> !cbuy dogecoin 1000
        Buys you dogecoin for 1000 USD.
        """
        try:
            buy_price = float(usd)
        except ValueError:
            return await ctx.send('Nice try, next time use a number for usd!')
        if buy_price < 0:
            return await ctx.send('I see what you did there ;) Don\'t buy for negative prices!')
        buy_price = round(buy_price, 2)
        try:
            price = await self.get_price(coin_id)
        except RequestException as e:
            return await ctx.send(f'Something went wrong: {e}')

        share_amount = buy_price / price
        self.buy_coin(ctx.author.id, coin_id, share_amount, buy_price)
        self.logger.info('%s just bought %f %s for %f usd', ctx.author, share_amount, coin_id, buy_price)
        await ctx.send(f'Successfully bought {share_amount} {coin_id} for {usd} USD.')

    @commands.command()
    async def csell(self, ctx, coin_id, share_amount=None):
        """
        Sell some coin

        Parameters
        ----------
        coin_id : str
            The ID of the coin you want to sell

        share_amount : float
            The amount of shares you want to sell

        Example
        -------
        >>> !csell dogecoin 100
        Sell 100 dogecoin shares for the current market price
        """
        if share_amount is None:
            share_amount = self.get_portfolio_value(ctx.author.id, coin_id)['amount']
        try:
            sell_amount = float(share_amount)
        except ValueError:
            return await ctx.send('The share amount has to be a number!')
        if sell_amount < 0:
            return await ctx.send('I see what you did there ;) Don\'t sell negative amounts of shares!')
        if sell_amount > self.get_portfolio_value(ctx.author.id, coin_id)['amount']:
            return await ctx.send(f'You don\'t have enough {coin_id} for that.')
        try:
            price = await self.get_price(coin_id)
        except RequestException as e:
            return await ctx.send(f'Something went wrogn: {e}')
        profit = round(price * sell_amount, 2)
        self.sell_coin(ctx.author.id, coin_id, sell_amount, profit)
        self.logger.info('%s just sold %f %s for %f usd', ctx.author, sell_amount, coin_id, profit)
        await ctx.send(f'Successfully sold {sell_amount} {coin_id} for {profit} USD.')

    @commands.command()
    async def cshow(self, ctx):
        """
        Show your current portfolio
        """
        self.ensure_portfolio(ctx.author.id)
        await ctx.channel.typing()
        if self.get_portfolio_value(ctx.author.id, 'usd_invested') == 0:
            return await ctx.reply('You have not invested in anything yet.')
        msg = f'Portfolio for {ctx.author.mention}\n```\n'
        global_invested = 0
        global_value = 0
        rows = []
        headers = ('Coin', 'Amount', 'Value', 'Change since Bought')
        for coin_id, coin in self.portfolios[ctx.author.id].items():
            if coin_id in ['usd_invested', 'usd_received'] or coin['amount'] == 0:
                continue
            usd_invested = coin['amount'] * coin['avg_buy_price']
            current_value = await self.get_price(coin_id) * coin['amount']
            pct_change = ((current_value - usd_invested) / usd_invested) * 100
            rows.append((coin_id, str(coin['amount']), str(current_value), f'{pct_change:.2f}'))
            global_invested += usd_invested
            global_value += current_value
        if global_invested != 0:
            pct_change = ((global_value - global_invested) / global_invested) * 100
        else:
            pct_change = 0
        invested = self.get_portfolio_value(ctx.author.id, 'usd_invested')
        withdrawn = self.get_portfolio_value(ctx.author.id, 'usd_received')
        table = tabulate(rows, headers=headers)
        msg += table
        msg += f'\n\nCurrent value: {global_value:.2f}\nOverall change: {pct_change:.2f}%.\nInvested: ${invested}, withdrawn: ${withdrawn}.\n'
        msg += '```'
        await ctx.send(msg)

    def load_data(self):
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couln't find %s directory, created new one.", PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, "r") as fp:
                data = yaml.safe_load(fp)
            self.portfolios = data
        else:
            self.save_data()
            self.logger.warning("Couln't find %s, created new one.", FILE_PATH)

    def save_data(self):
        with open(FILE_PATH, 'w') as fp:
            yaml.safe_dump(self.portfolios, fp)


async def setup(bot: Bot):
    await bot.add_cog(StocksCog(bot))
