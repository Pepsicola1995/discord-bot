from __future__ import annotations
import datetime
import logging
import math
import random
import sqlite3
from functools import partial
from functools import reduce
from operator import mul
from typing import Optional

import discord
from discord import app_commands
from discord.ext import commands
from discord.utils import format_dt
from discord.utils import snowflake_time
from sqlalchemy import select
from sqlalchemy import func
from sqlalchemy.ext.asyncio import AsyncSession
from tabulate import tabulate

import db
from bot import Bot
from config import Config
from db.testing import Base
from db.testing import CountClick
from db.testing import CountMeta
from db.testing import Roll


async def get_clicker(clicker_id: int, session: AsyncSession) -> CountClick:
    stmt = select(CountClick).where(CountClick.discord_id == clicker_id)
    async with session.begin():
        clicker = (await session.execute(stmt)).scalars().first()
    if clicker is None:
        clicker = CountClick(discord_id=clicker_id, amount=0)
    return clicker


async def get_clickers(session: AsyncSession) -> list[CountClick]:
    stmt = select(CountClick).order_by(CountClick.amount.desc())
    async with session.begin():
        clickers = (await session.execute(stmt)).scalars().all()
    return clickers


async def get_last_clicker(session: AsyncSession) -> int:
    async with session.begin():
        count_meta = (await session.execute(select(CountMeta))).scalars().first()
    return count_meta.last_click_id


async def get_current_count(session: AsyncSession) -> int:
    async with session.begin():
        count_meta = (await session.execute(select(CountMeta))).scalars().first()
    return count_meta.count


async def increment_count(clicker_id: int, session: AsyncSession) -> int:
    async with session.begin():
        count_meta = (await session.execute(select(CountMeta))).scalars().first()
    clicker = await get_clicker(clicker_id, session)
    async with session.begin():
        count_meta.count += 1
        count_meta.last_click_id = clicker_id
        clicker.amount += 1
        session.add(clicker)
        session.add(count_meta)
    return count_meta.count


def stndrdth(number: int) -> str:
    last_two_digits = number % 100
    if last_two_digits in (11, 12, 13):
        return "th"
    last_digit = number % 10
    return {1: 'st', 2: 'nd', 3: 'rd'}.get(last_digit, 'th')


def prime_sieve(n: int, primes: list[bool]):
    primes.clear()
    # Have to use a for-loop because we want to alter the primes object
    for _ in range(n + 1):
        primes.append(True)
    primes[0] = False
    primes[1] = False
    for p in range(2, int(math.sqrt(n))):
        if (primes[p] is True):
            for i in range(p * p, n + 1, p):
                primes[i] = False


class LinkButton(discord.ui.View):
    def __init__(self, link: str):
        super().__init__()
        self.add_item(discord.ui.Button(label="Can't see a picture? Click me!", url=link))

class PersistentCountButton(discord.ui.View):
    def __init__(self, current_count: int, bot: Bot):
        super().__init__(timeout=None)
        btn = discord.ui.Button(label=str(current_count),
                                style=discord.ButtonStyle.green,
                                custom_id='persistent_count_button')
        btn.callback = partial(self.count, button=btn)
        self.prime_table = []
        # fills the prime_table
        prime_sieve(1_000_000, self.prime_table)
        self.prime_list = [i for i in range(1_000_000) if self.prime_table[i]]
        self.bot = bot
        self.add_item(btn)

    async def get_cat_picture_link(self) -> str:
        url = 'https://api.thecatapi.com/v1/images/search'
        params = {'api_key': self.bot.config.catapi_key}
        async with self.bot.aiohttp_session.get(url, params=params) as resp:
            return (await resp.json())[0]['url']

    async def send_cat_picture(self, interaction: discord.Interaction) -> None:
        link = await self.get_cat_picture_link()
        emb = discord.Embed(title='Thank you for your participation!',
                            description='Have a cat :3',
                            colour=interaction.user.color)
        emb.set_image(url=link)
        await interaction.followup.send(embed=emb, ephemeral=True, view=LinkButton(link))

    async def send_bonus_message(self, interaction: discord.Interaction, count: int) -> None:
        faculties = {reduce(mul, range(1, i)): i for i in range(2, 10)}
        if count > 9 and len(set(str(count))) == 1:
            await interaction.followup.send('Nice number! 🥳', ephemeral=True)
        elif count == 420:
            await interaction.followup.send('Blaze it 😎', ephemeral=True)
        elif count % 1000 == 0:
            await interaction.followup.send(
                f'Successfully counted to {count}, what a god!', ephemeral=True
            )
        elif count % 100 == 0:
            await interaction.followup.send(f'Good job, you made it to {count} 👍', ephemeral=True)
        elif count in (2**i for i in range(15)):
            await interaction.followup.send(f'Thats {bin(count)}', ephemeral=True)
        elif count in faculties.keys():
            await interaction.followup.send(f'Thats {int(faculties[count])}!', ephemeral=True)
        elif math.sqrt(count) == int(math.sqrt(count)):
            await interaction.followup.send(f'Thats {int(math.sqrt(count))}²', ephemeral=True)
        elif self.prime_table[count]:
            nth_prime = self.prime_list.index(count)
            await interaction.followup.send(
                f"That's the {nth_prime}{stndrdth(nth_prime)} prime number!", ephemeral=True
            )

    async def count(self, interaction: discord.Interaction, button: discord.ui.Button):
        async with db.async_sessionmaker() as session:
            last_clicker = await get_last_clicker(session)
            if last_clicker == interaction.user.id:
                await interaction.response.send_message('You can not count multiple times in a row!',
                                                        ephemeral=True)
                return
            new_count = await increment_count(interaction.user.id, session)
        button.label = str(new_count)
        button.style = random.choice(button_styles)
        await interaction.response.edit_message(view=self)
        await self.send_cat_picture(interaction)
        await self.send_bonus_message(interaction, new_count)


button_styles = [
    discord.ButtonStyle.blurple,
    discord.ButtonStyle.red,
    discord.ButtonStyle.green,
    discord.ButtonStyle.gray,
]


class CustomButton(discord.ui.View):
    def __init__(self, label: str):
        super().__init__()
        btn = discord.ui.Button(label=label, style=discord.ButtonStyle.green)
        btn.callback = partial(self.callback, button=btn)
        self.add_item(btn)
        self.clicks = 0

    async def callback(self, interaction: discord.Interaction, button: discord.ui.Button):
        button.style = random.choice(button_styles)
        self.clicks += 1
        if self.clicks >= 3:
            button.disabled = True
            self.stop()
        await interaction.response.send_message('Hey, good job clicking that button!', ephemeral=True)
        await interaction.followup.edit_message(interaction.message.id, view=self)


class Counter(discord.ui.View):
    @discord.ui.button(label='0', style=discord.ButtonStyle.blurple)
    async def count(self, interaction: discord.Interaction, button: discord.ui.Button):
        next_number = int(button.label) + 1
        button.label = str(next_number)
        button.style = random.choice(button_styles)
        if next_number >= 10:
            button.disabled = True
            self.stop()
        await interaction.response.edit_message(view=self)


class ReportDropdown(discord.ui.Select):
    def __init__(self, member: discord.Member, options: list[discord.SelectOption]):
        super().__init__(placeholder=f'Choose a reason for reporting {member}',
                         min_values=1,
                         max_values=1,
                         options=options)
        self.reported_member = str(member)
        self.logger = logging.getLogger(__name__)

    async def callback(self, interaction: discord.Interaction):
        self.disabled = True
        await interaction.response.edit_message(
            content=f'Successfully reported {interaction.user.mention} for being a little weeb shit.'
                    '\nThis report is anonymous.'
                    '\n**The council has been notified.**',
            view=None
        )
        self.logger.info('%s has reported %s for %s',
                         interaction.user,
                         self.reported_member,
                         self.values[0])


class ReportDropdownView(discord.ui.View):
    def __init__(self, member: discord.Member, cfg: Config):
        super().__init__()
        options = [
            discord.SelectOption(label=d['label'], description=d['description'])
            for d in cfg.report_reasons
        ]
        self.add_item(ReportDropdown(member, options))


class TestingCog(commands.Cog):
    """
    Cogs for all sorts of random commands

    Used to actually be to test commands, but now its just a collection of
    uncategorizeable commands
    """

    def __init__(self, bot: Bot, current_count: int):
        self.bot = bot
        self.rouletters = {}
        self.logger = logging.getLogger(__name__)
        self.report_ctx_menu = app_commands.ContextMenu(name='report', callback=self.ctx_report)
        self.bot.tree.add_command(self.report_ctx_menu)
        self.bot.add_view(PersistentCountButton(current_count, bot))

    # def reset_count(self):
    #     self.count = 0
    #     self.last_click_id = None
    #     self.save_data()

    def build_timedelta_string(self, td: datetime.timedelta):
        years = td.days // 365
        months = (td.days % 365) // 30
        days = ((td.days % 365) % 30)
        hours = td.seconds // 3600
        minutes = (td.seconds % 3600) // 60
        seconds = ((td.seconds % 3600) % 60)
        ret = ""
        if years:
            ret += f"{years} years, "
        if months:
            ret += f"{months} months, "
        if days:
            ret += f"{days} days, "
        if hours:
            ret += f"{hours} hours, "
        if minutes:
            ret += f"{minutes} minutes, "
        if seconds:
            ret += f"{seconds} seconds, "
        if not ret:
            return ""
        return ret[:-2]

    async def add_roll(self, roller_id: int, rolled_id: int, session: AsyncSession):
        async with session.begin():
            roll = Roll(timestamp=datetime.datetime.now(), roller_id=roller_id, rolled_id=rolled_id)
            session.add(roll)

    async def get_roll_amount(self, roller_id: int, session: AsyncSession):
        stmt = select(func.count(Roll.id)).where(Roll.roller_id == roller_id)
        async with session.begin():
            count = (await session.execute(stmt)).scalars().first()
        return count

    @commands.command()
    async def roulette(self, ctx: commands.Context):
        if not isinstance(ctx.channel, discord.TextChannel) and not isinstance(ctx.channel, discord.Thread):
            return
        members = []
        async with db.async_sessionmaker() as session:
            for member in ctx.guild.members:
                roll_count = await self.get_roll_amount(member.id, session)
                members += [member] * (roll_count + 1)
            chosen = random.choice(members)
            await self.add_roll(ctx.author.id, chosen.id, session)
        self.logger.info('%s - %d rolled %s - %d in %s/#%s',
                         ctx.author.display_name,
                         ctx.author.id,
                         chosen.display_name,
                         chosen.id,
                         ctx.guild.name,
                         ctx.channel.name)
        await ctx.send(f'{ctx.author.display_name} rolled {chosen.mention}')


    @commands.command()
    async def dts(self, ctx, obj: discord.Object):
        creation_time = snowflake_time(obj.id)
        delta = datetime.datetime.now(datetime.timezone.utc) - creation_time
        return await ctx.send(f'{format_dt(creation_time)}, that is {format_dt(creation_time, style="R")} or `{delta.days}` days ago.')

    @commands.command()
    async def ping(self, ctx):
        """pong"""
        return await ctx.send("pong")

    @commands.command()
    async def cattp(self, ctx, code=None):
        """:3"""
        if code is None:
            return await ctx.send_help(ctx.command)
        return await ctx.send(f"https://http.cat/{code}")

    @commands.command()
    @commands.is_owner()
    async def download(self, ctx, channelid):
        LIMIT = None
        if not channelid.isdigit():
            return await ctx.send("channelid must be an int")
        channelid = int(channelid)
        try:
            channel = self.bot.get_channel(channelid)
        except Exception as exc:
            return await ctx.send(f"Did not work: {exc}")
        await ctx.send(f'Downloading from <#{channelid}>')
        conn = sqlite3.connect(f'data/messages.db')
        c = conn.cursor()
        c.execute(f'CREATE TABLE IF NOT EXISTS {channel.name.replace("-", "")}'
                  '(userid TEXT, usertag TEXT, message TEXT)')
        c.execute(f'CREATE TABLE IF NOT EXISTS global(userid TEXT, usertag '
                  'TEXT, message TEXT)')
        i = 0
        async for message in channel.history(limit=LIMIT):
            content = message.content.replace("\"", "''")
            c.execute(f'INSERT INTO {channel.name.replace("-", "")} VALUES('
                      f'"{str(message.author.id)}", "{str(message.author)}", '
                      f'"{content}")')
            c.execute(f'INSERT INTO global VALUES("{str(message.author.id)}", '
                      f'"{str(message.author)}", "{content}")')
            i += 1
            if i % 1000 == 0:
                conn.commit()
            if i % 10000 == 0:
                self.logger.info('Got %i messages so far...', i)
        conn.commit()
        c.close()
        conn.close()
        return await ctx.send(f'{ctx.author.mention} all done.')


    @commands.command(hidden=True)
    async def rewards(self, ctx):
        """
        If you execute this command :JabOkay: will haunt you for 7 years.
        """
        await ctx.author.send("Congratulations!\nYou have been rewarded a "
                              "Rocket League Fan Rewards drop!")
        self.logger.info('%s - %s rewarded themselves in channel %s - %s',
                         ctx.author,
                         ctx.author.id,
                         ctx.channel,
                         ctx.channel.id)


    @commands.command(aliases=['avatar'])
    async def pfp(self, ctx: commands.Context, *, user: Optional[discord.Member]=None):
        """
        Show the pfp/avatar of a user

        Parameters
        ----------
        user : Member
            The user of which you want to show the pfp. If none provided, shows your own pfp.

        Example
        -------
        >>> !pfp gaben
        Shows you the pfp of the user gaben, if that user exists.
        """
        if user is None:
            user = ctx.author
        await ctx.send(user.display_avatar.url)

    @commands.command()
    async def userinfo(self, ctx, *, user: Optional[discord.Member]=None):
        """
        Display info about your or someone elses discord account.

        Parameters
        ----------
        user : Member
            The user of which you want to get the info. If none provided, shows your own info.

        Example
        -------
        >>> !userinfo 123456
        Shows you some info about the account of user 123456
        """
        if user is None:
            user = ctx.author
        fullname = str(user)
        roles = [str(role) for role in user.roles]
        colour = user.colour
        avatar = user.display_avatar.url
        embed = discord.Embed(title=fullname, colour=colour)
        embed.add_field(name="Joined Discord",
                        value='\n'.join((format_dt(user.created_at, style='R'),
                                         format_dt(user.created_at, style='f'))),
                        inline=True)
        embed.add_field(name=f"Joined *{ctx.guild.name}*",
                        value='\n'.join((format_dt(user.joined_at, style='R'),
                                         format_dt(user.joined_at, style='f'))),
                        inline=True)
        embed.add_field(name="Roles", value=", ".join(roles), inline=False)
        embed.set_thumbnail(url=avatar)
        embed.set_footer(text=f"User ID: {user.id}")
        await ctx.send("", embed=embed)
        self.logger.info('%s - %s got the userinfo of user %s in channel %s - %s',
                         ctx.author,
                         ctx.author.id,
                         user.id,
                         ctx.channel,
                         ctx.channel.id)

    @commands.command()
    async def choose(self, ctx, *args: commands.clean_content):
        """
        Choose a random item from a given list of items

        Parameters
        ----------
        args : list
            a SPACE SEPERATED list of items
            an item that contains a space must be surrounded by ""

        Example
        -------
        >>> !choose hello darkness my old friend
        Says a random word of the list "hello" "darkness" "my" "old" "friend"
        """
        if not args:
            return await ctx.send_help(ctx.command)
        choice = random.choice(args)
        emb = discord.Embed(color=ctx.author.color)
        emb.set_author(name=str(ctx.author), icon_url=ctx.author.display_avatar.url)
        emb.description = choice
        await ctx.send(embed=emb)
        self.logger.info('%s - %s chose a random item in channel %s - %s. Choice: %s',
                         ctx.author,
                         ctx.author.id,
                         ctx.channel,
                         ctx.channel.id,
                         choice)

    @commands.command(aliases=["6mans"])
    async def sixmans(self, ctx, *args: commands.clean_content):
        """
        Choose 2 captains and a first pick from a list of players

        Parameters
        ----------
        players : list
            a SPACE SEPERATED list of players
            a player that has a space in his name must be surrounded by ""

        Example
        -------
        >>> !sixmans ghost polders howdy scheist darxen jabbert
        Gives you an embed showing the two picked captains, and the person
        that picks first
        """
        if not args or len(args) < 2:
            return await ctx.send_help(ctx.command)
        args = list(args)
        cap1 = random.choice(args)
        args.remove(cap1)
        cap2 = random.choice(args)
        caps = [cap1, cap2]
        firstpick = random.choice(caps)
        embed = discord.Embed(color=ctx.author.color)
        embed.set_author(name=str(ctx.author), icon_url=ctx.author.display_avatar.url)
        embed.add_field(name="**Captain 1:**", value=cap1)
        embed.add_field(name="**Captain 2:**", value=cap2)
        embed.add_field(name="**First Pick:**", value=firstpick)
        embed.set_thumbnail(url="https://rocketleague.market/images/items/"
                                "body_octane.png")
        await ctx.send(None, embed=embed)
        self.logger.info('%s - %s sixmaned in channel %s - %s. Cap1: %s, Cap2: %s, FP: %s',
                         ctx.author,
                         ctx.author.id,
                         ctx.channel,
                         ctx.channel.id,
                         cap1,
                         cap2,
                         firstpick)


    @commands.command()
    async def codeblock(self, ctx: discord.Context):
        embed = discord.Embed(color=discord.Color.from_str('#00b4cc'))
        content = """
\```language
<code>
\```
**Example**
\```python
print("Hello World")
\```
*turns into*
```python
print("Hello World")
```
You can use any popular language. E.g:
```
c, c++, css, diff, html, js, java, php, python, sql
```
        """
        embed.add_field(name='How to Code Block', value=content)
        await ctx.send(embed=embed)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def repeat(self, ctx, *, msg=None):
        """
        Repeat a message in the same channel.

        Parameters
        ----------
        msg : str
            the message you want to repeat

        Example
        -------
        >>> !repeat hello friends :)
        Says "hello friends :)" in the same channel that the command was used
        in.
        """
        if msg is None:
            return await ctx.send_help(ctx.command)
        await ctx.send(msg)

    @commands.command(aliases=["rrepeat"], hidden=True)
    @commands.is_owner()
    async def remote_repeat(self, ctx, channel_id=None, *, msg=None):
        """
        Repeat your message in a given channel

        Parameters
        ----------
        channel_id : int
            the id of the channel you want to repeat the message to
        msg : str
            the message you want to repeat

        Example
        -------
        >>> !rrepeat 12343045987 hello friends :)
        Says the message "hello friends :)" to the channel 12343045987
        """
        if channel_id is None or msg is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify a channel_id and a "
                                  "message to repeat.")
        if not channel_id.isdigit():
            await ctx.send_help(ctx.command)
            return await ctx.send("`channelid` must be an int.")
        channel = self.bot.get_channel(int(channel_id))
        await channel.send(msg)

    @commands.command()
    @commands.is_owner()
    async def react(self, ctx: commands.Context, message_id: int, *, reaction: str):
        await ctx.invoke(self.remote_react,
                         channel_id=ctx.channel.id,
                         message_id=message_id,
                         reaction=reaction)

    @commands.command(aliases=['rreact'], hidden=True)
    @commands.is_owner()
    async def remote_react(self, ctx, channel_id: int, message_id: int, *, reaction: str):
        channel = self.bot.get_partial_messageable(channel_id)
        msg = channel.get_partial_message(message_id)
        for character in reaction:
            try:
                await msg.add_reaction(character)
            except:
                pass
        await ctx.message.add_reaction('👍')

    @commands.guild_only()
    @commands.command(aliases=['count'])
    async def countto1000(self, ctx: commands.Context):
        async with db.async_sessionmaker() as session:
            current_count = await get_current_count(session)
        await ctx.send(view=PersistentCountButton(current_count, self.bot))

    @commands.guild_only()
    @commands.is_owner()
    @commands.command()
    async def count_numbers(self, ctx: commands.Context):
        async with db.async_sessionmaker() as session:
            clickers = await get_clickers(session)
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        rows = [(str(guild.get_member(clicker.discord_id)), clicker.amount) for clicker in clickers]
        table = tabulate(rows, headers=('User', 'Clicks'))
        await ctx.send(f'```{table}```')

    # @commands.command(name='reset_count')
    # @commands.is_owner()
    # async def cmd_reset_count(self, ctx: commands.Context):
    #     self.reset_count()
    #     await ctx.send('Successfully reset the count :)')

    @commands.command()
    async def button(self, ctx: commands.Context, label: Optional[str] = None):
        if label is None:
            await ctx.send('Push me!', view=Counter())
        else:
            await ctx.send(view=CustomButton(label))

    @commands.command()
    @commands.is_owner()
    async def report(self, ctx: commands.Context, member: discord.Member):
        await ctx.send(view=ReportDropdownView(member, self.bot.config))

    async def ctx_report(self, interaction: discord.Interaction, member: discord.Member):
        await interaction.response.send_message(view=ReportDropdownView(member, self.bot.config),
                                                ephemeral=True)


    # events
    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Show zero tolerance for the nini meme
        """
        msg_content = message.content
        if msg_content.strip().lower() == "nini":
            await message.channel.send("danNo")


async def setup(bot: Bot):
    async with db.async_sessionmaker() as session:
        current_count = await get_current_count(session)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(TestingCog(bot, current_count))
