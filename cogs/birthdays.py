from __future__ import annotations

import datetime
import logging
import os
import sys
import traceback
from dataclasses import dataclass
from typing import Optional

import dateutil
import yaml
from discord.ext import commands, tasks
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.locale import Locale

BIRTHDAY_DATA_NAME = "birthdays.yml"
PATH = "data/birthdays/"
BIRTHDAY_DATA_PATH = PATH + BIRTHDAY_DATA_NAME
MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
}


@dataclass
class Birthday:
    day: int
    month: int

    def to_dict(self) -> dict[str, int]:
        return {'day': self.day, 'month': self.month}

    def __str__(self):
        return f'{MONTHS[self.month]} {self.day}{stndrdth(self.day)}'


class InvalidDateError(ValueError):
    def __init__(self, value):
        self.value = value
        super().__init__(f'The value {value} is not a valid date in MMDD format!')


def stndrdth(number: int) -> str:
    """
    Get the proper postfix for a given number

    Parameters
    ----------
    number : int
        the number you want to get the postfix of
    """
    strnr = str(int(number))
    if strnr[-2:] in ["11", "12", "13"]:
        return "th"
    if strnr[-1:] == "1":
        return "st"
    if strnr[-1:] == "2":
        return "nd"
    if strnr[-1:] == "3":
        return "rd"
    return "th"


class BirthdayCog(commands.Cog):
    """
    Cog for birthday commands

    Allows users to store their birthdays and timezones,
    then the bot will 'congratulate' the person in the
    defined channel ~close to 00:00 in their timezone.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.congratulation_channel_id = self.bot.config.ids.tavern_id
        self.birthday_dict: dict[int, Birthday] = {}
        self.congratulated: list[int] = []
        self.load_data()
        self.restore_birthday_task.start()
        self.check_birthday_task.start()

    async def cog_unload(self):
        self.restore_birthday_task.cancel()
        self.check_birthday_task.cancel()

    async def get_locale(self, session: AsyncSession, user_id: int) -> Optional[Locale]:
        stmt = select(Locale).where(Locale.id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    @commands.group(aliases=["birthday"])
    async def birthdays(self, ctx):
        """
        Group for birthday commands.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @birthdays.command(name='set')
    async def set_birthday(self, ctx, date=None):
        """
        Add your birthday to the birthday list.

        Parameters
        ----------
        date : int
            your date of birth in the format MMDD

        Example
        -------
        >>> !birthday set 0311
        Sets march 11th as your birthday
        """
        if date is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify the date in the format "
                                  "MMDD and the timezone of your birthday.")
        if len(date) != 4 or not date.isdigit():
            raise InvalidDateError(date)
        month = int(date[:2])
        day = int(date[2:])
        if not (1 <= day <= 31) or not (1 <= month <= 12):
            raise InvalidDateError(date)

        birthday = Birthday(day, month)
        self.birthday_dict[ctx.author.id] = birthday
        self.save_data()
        self.logger.info('%s - %d set their birthday to `%s`', ctx.author, ctx.author.id, birthday)

        msg = f'Successfully set your birthday to {birthday}.'
        async with db.async_sessionmaker() as session:
            locale = await self.get_locale(session, ctx.author.id)
        if locale is None:
            msg += ('\nYou might want to consider setting a locale via `!locale set` so that you '
                    'will be congratulated in the correct timezone.')
        await ctx.send(msg)

    @set_birthday.error
    async def set_birthday_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, InvalidDateError):
            await ctx.send(f'The date {error.value} is not a valid MMDD date.')
        else:
            print(f'Ignoring exception in command {ctx.command}:', file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @birthdays.command(aliases=["del", "delete", "rem"])
    async def remove(self, ctx):
        """
        Remove your birthday from the birthday list
        """
        ret = self.birthday_dict.pop(ctx.author.id, -1)
        if ret == -1:
            await ctx.send("You didn't register your birthday yet.")
        else:
            self.save_data()
            self.logger.info('%s removed their birthday.', ctx.author)
            await ctx.send("Successfully removed your birthday from the birthday list.")

    @birthdays.command()
    async def show(self, ctx):
        """
        Show the birthday thats currently set for your discord account.
        """
        if ctx.author.id not in self.birthday_dict:
            return await ctx.send('Oops, you forgot to register your birthday.')
        birthday = self.birthday_dict[ctx.author.id]
        await ctx.send(f'You have set your birthday to {birthday}')

    async def congratulate(self, userid):
        """
        Send a congratulation message for the user to #tavern in the JabKingdom

        Parameters
        ----------
        userid : int
            the discordid of the user who is congratulated.
        """
        channel = self.bot.get_channel(self.congratulation_channel_id)
        self.logger.info("Congratulated %s", userid)
        await channel.send(f"Happy birthday <@{userid}>")

    # loops

    async def get_tzinfo(self, session: AsyncSession, user_id: int) -> datetime.tzinfo:
        locale = await self.get_locale(session, user_id)
        if locale is None:
            return dateutil.tz.UTC
        return dateutil.tz.gettz(locale.locale)

    @tasks.loop(seconds=3600.0)
    async def restore_birthday_task(self):
        """
        Check every hour whether someones birthday has recently ended
        """
        async with db.async_sessionmaker() as session:
            for user_id in self.congratulated:
                user_locale = await self.get_tzinfo(session, user_id)
                local_datetime = datetime.datetime.now(tz=user_locale)
                birthday = self.birthday_dict[user_id]
                if local_datetime.day != birthday.day or local_datetime.month != birthday.month:
                    self.congratulated.remove(user_id)
                    self.logger.info("%s birthday has ended", user_id)
                    self.save_data()

    @tasks.loop(seconds=60.0)
    async def check_birthday_task(self):
        """
        Check whether someones birthday has started every minute.

        Parameters
        ----------
        client : discord.Client()
            the client that sends out birthday congratulations
        """
        async with db.async_sessionmaker() as session:
            for user_id in self.birthday_dict:
                if user_id in self.congratulated:
                    continue
                user_locale = await self.get_tzinfo(session, user_id)
                local_datetime = datetime.datetime.now(tz=user_locale)
                birthday = self.birthday_dict[user_id]
                if local_datetime.month == birthday.month and local_datetime.day == birthday.day:
                    await self.congratulate(user_id)
                    self.congratulated.append(user_id)
                    self.save_data()

    @check_birthday_task.before_loop
    async def before_congratulating(self):
        await self.bot.wait_until_ready()

    def load_data(self):
        """
        Load the birthay dictionary
        """
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one.", PATH)
        if os.path.isfile(BIRTHDAY_DATA_PATH):
            with open(BIRTHDAY_DATA_PATH, "r", encoding='utf-8') as fp:
                dct = yaml.safe_load(fp)
                self.birthday_dict = {user_id: Birthday(**birthday_dict)
                                      for user_id, birthday_dict in dct['birthdays'].items()}
                self.congratulated = dct["congratulated"]
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one.", BIRTHDAY_DATA_PATH)

    def save_data(self):
        """
        Save the current state of the birthday dictionary
        """
        bday_dict = {user_id: birthday.to_dict()
                     for user_id, birthday in self.birthday_dict.items()}
        dct = {
            "birthdays": bday_dict,
            "congratulated": self.congratulated
        }
        with open(BIRTHDAY_DATA_PATH, "w", encoding='utf-8') as fp:
            yaml.safe_dump(dct, fp)


async def setup(bot: Bot):
    await bot.add_cog(BirthdayCog(bot))
