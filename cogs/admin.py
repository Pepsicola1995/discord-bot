import asyncio
import discord
import logging
import os
import traceback
from typing import Optional

from discord.ext import commands

from bot import Bot


class AdminCog(commands.Cog):
    """
    Cog for admin commands

    Mostly for bot management and deletion of messages
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.command()
    @commands.guild_only()
    @commands.is_owner()
    async def sync(self, ctx: commands.Context):
        async with ctx.channel.typing():
            self.bot.tree.copy_global_to(guild=ctx.guild)
            await ctx.bot.tree.sync(guild=ctx.guild)
        await ctx.send('Successfully synced all app commands to the current guild.')

    @commands.command()
    @commands.is_owner()
    async def play(self, ctx, *, text=None):
        """
        Set the bots playing state

        Parameters
        ----------
        text : str
            The 'game' you want to set the bot to

        Examples
        --------
        >>> !play bullying polders
        Sets the bots playing state to 'bullying polders'
        """
        await self.bot.change_presence(activity=discord.Game(name=text.strip(), type=1))
        self.logger.info('Set playing status to "%s".', text)
        await ctx.send(f"Successfully changed the playing game to: **{text}**")

    @commands.command()
    @commands.is_owner()
    async def purge_me(self, ctx, limit: Optional[int] = None):
        count = 0
        async for message in ctx.channel.history(limit=limit):
            if message.author == self.bot.user:
                await message.delete()
                count += 1
        msg = await ctx.channel.send(f'Successfully deleted {count} messages')
        await asyncio.sleep(10)
        await msg.delete()

    @commands.command()
    @commands.is_owner()
    async def load(self, ctx, *, module=None):
        """
        Load a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>> !load admin
        Loads the admin module
        """
        if module is None:
            return await ctx.send("You need to specify a module to load.")
        try:
            await self.bot.load_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f'```py\n{traceback.format_exc()}\n```')
        else:
            await ctx.send(f"Successfully loaded `cogs.{module}`")
            self.logger.info("Successfully loaded `cogs.%s`", module)

    @commands.command()
    @commands.is_owner()
    async def unload(self, ctx, *, module=None):
        """
        Unload a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>>!unload admin
        Unloads the admin module
        """
        if module is None:
            return await ctx.send("You need to specify a module to unload")
        try:
            await self.bot.unload_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f'```py\n{traceback.format_exc()}\n```')
        else:
            await ctx.send(f"Successfully unloaded `cogs.{module}`")
            self.logger.info("Successfully unloaded `cogs.%s`", module)

    @commands.command(name='reload')
    @commands.is_owner()
    async def _reload(self, ctx, *, module=None):
        """
        Reload a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>> !reload admin
        Reloads the admin module
        """
        if module is None:
            return await ctx.send("You need to specify a module to reload")
        if module == "*":
            for cog in self.bot.extensions.keys():
                await self.bot.unload_extension(cog)
                await self.bot.load_extension(cog)
            self.logger.info("Successfully reloaded all cogs.")
            return await ctx.send("Successfully reloaded all cogs.")
        try:
            await self.bot.unload_extension(f"cogs.{module}")
            await self.bot.load_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f'```py\n{traceback.format_exc()}\n```')
        else:
            await ctx.send(f"Successfully reloaded `cogs.{module}`")
            self.logger.info("Successfully reloaded `cogs.%s`", module)

    @commands.command(name="list_cogs")
    @commands.is_owner()
    async def list_cogs(self, ctx):
        """
        List all available cogs
        """
        files = os.listdir("cogs/")
        modules = []
        for item in files:
            if ".py" in item:
                modules.append(item[:-3])
        msg = "\n".join(modules)
        return await ctx.send(f"```\n{msg}\n```")

    @commands.command()
    @commands.is_owner()
    async def bye(self, ctx, msg_id=None):
        """
        Delete a message by messageid
        Parameters
        ----------
        msg_id : int
            the id of the message you want to delete

        Examples
        --------
        >>> !bye 1232432138320999
        Deletes the message with the id 1232432139320999
        """
        if msg_id is None:
            return await ctx.send("You need to specify a message to delete.")
        if not msg_id.isdigit():
            return await ctx.send("`msg_id` must be an int.")
        msg = ctx.channel.get_partial_message(int(msg_id))
        try:
            await msg.delete()
        except discord.NotFound:
            await ctx.send('That message does not seem to exist.')
            return
        self.logger.info('Deleted message with id `%s` in channel `%s`', msg_id, ctx.channel.name)

    @commands.command(aliases=["rbye"])
    @commands.is_owner()
    async def bye_adv(self, ctx, channel_id=None, msg_id=None):
        """
        Remotely delete a message

        Parameters
        ----------
        channel_id : int
            the id of the channel that contains the message you want to delete
        msg_id : int
            the id of the message that you want to delete

        Examples
        --------
        >>> !bye_adv 1232432138320999 3232432138320000
        Deletes the message with the id 1232432138320999 the channel with the
        id 3232432138320000
        """
        if channel_id is None or msg_id is None:
            return await ctx.send("You need to specify a channel and a "
                                  "message to delete.")
        if not channel_id.isdigit():
            return await ctx.send("`channel_id` must be an int.")
        if not msg_id.isdigit():
            return await ctx.send("`msg_id` must be an int.")
        channel = self.bot.get_partial_messageable(int(channel_id))
        msg = channel.get_partial_message(int(msg_id))
        try:
            await msg.delete()
        except discord.NotFound:
            await ctx.send('That message does not seem to exist.')
            return
        self.logger.info('Deleted message with id `%s` in channel `%s`.', msg_id, channel.name)

    @commands.command(aliases=["redit"])
    @commands.is_owner()
    async def remote_edit(self, ctx, channel_id=None, msg_id=None, *, new_content=None):
        if channel_id is None or msg_id is None:
            return await ctx.send("You need to specify a channel and a "
                                  "message to delete.")
        if not channel_id.isdigit():
            return await ctx.send("`channel_id` must be an int.")
        if not msg_id.isdigit():
            return await ctx.send("`msg_id` must be an int.")
        channel = self.bot.get_partial_messageable(int(channel_id))
        msg = channel.get_partial_message(int(msg_id))
        try:
            await msg.edit(content=new_content)
        except discord.NotFound:
            await ctx.send('That message does not seem to exist.')
            return
        self.logger.info('Edited message with id `%s` in channel `%s`.', msg_id, channel.name)

    @commands.command()
    @commands.is_owner()
    async def log(self, ctx, rows=20):
        """Show the last <rows> rows of the bot log"""
        with open('bot.log', 'r', encoding='utf-8') as fp:
            lines = fp.readlines()
        relevant_lines = ''.join(lines[-rows:])
        messages = self.paginate(relevant_lines)
        for message in messages:
            await ctx.send(f'```\n{message}\n```')

    def paginate(self, message):
        """
        split message into chunks of < 2000, only breaking at newlines, if possible.
        """
        messages = []
        while len(message) > 2000:
            pos = 1997
            while message[pos] != '\n':
                pos -= 1
                # if we can not split by newlines
                if pos < 30:  # 30 to keep at least a reasonable minimum line length
                    pos = 1997
                    break
            messages.append(message[:pos])
            message = message[pos:]
        messages.append(message)
        return messages


async def setup(bot: Bot):
    await bot.add_cog(AdminCog(bot))
