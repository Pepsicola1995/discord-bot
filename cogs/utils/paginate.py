def paginate_codeblock(msg: str) -> list[str]:
    msgs = []
    while len(msg) > 2000:
        i = 1987
        while msg[i] != '\n':
            i -= 1
        msgs.append(msg[:i] + '```')
        msg = '```' + msg[i+1:]
    msgs.append(msg)
    return msgs
