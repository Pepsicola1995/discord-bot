from dataclasses import dataclass

import asyncio
import logging
import time
from typing import Optional

import discord
from discord.ext import commands, tasks
import requests
import sqlalchemy
from sqlalchemy import select
from sqlalchemy.orm import Session

import db
from bot import Bot
from db.twitch import Base
from db.twitch import OauthToken
from db.twitch import Streamer
from db.twitch import Subscription


@dataclass
class TwitchStream:
    game_id: str
    game_name: str
    id: str
    is_mature: bool
    language: str
    started_at: str
    tag_ids: list[str]
    thumbnail_url: str
    title: str
    type: str
    user_id: str
    user_login: str
    user_name: str
    viewer_count: int


@dataclass
class TwitchUser:
    broadcaster_type: str
    created_at: str
    description: str
    display_name: str
    id: str
    login: str
    offline_image_url: str
    profile_image_url: str
    type: str
    view_count: int


@dataclass
class TwitchGame:
    box_art_url: str
    id: str
    name: str

# TODO: use per-command sessions
class Twitch:
    def __init__(self, bot: Bot):
        self.logger = logging.getLogger(__name__)
        self.db_session = db.async_sessionmaker
        self.aiohttp_session = bot.aiohttp_session
        self.base_url = 'https://api.twitch.tv/helix'
        self.oauth_token = self.load_oauth_token()
        self.token = self.load_oauth_token()
        self.config = bot.config

    def _blocking_get_oauth_token(self):
        url = 'https://id.twitch.tv/oauth2/token'
        params = {'client_id': self.config.twitch_id,
                  'client_secret': self.config.twitch_secret,
                  'grant_type': 'client_credentials'}
        resp = requests.post(url, params=params)
        return resp.json()['access_token']

    def load_oauth_token(self) -> str:
        with Session(db.BLOCKING_ENGINE) as session:
            token = session.execute(select(OauthToken.token)).scalars().first()
        if token is None:
            token = self._blocking_get_oauth_token()
            session.add(OauthToken(token=token))
            session.commit()
        return token

    async def get_oauth_token(self) -> OauthToken:
        async with self.db_session() as session:
            return (await session.execute(select(OauthToken))).scalars().first()

    async def set_oauth_token(self, token: str) -> None:
        oauth_token = await self.get_oauth_token()
        oauth_token.token = token
        self.token = token
        async with self.db_session() as session:
            async with session.begin():
                session.add(oauth_token)

    async def refresh_oauth_token(self):
        """Gets a new oauth token from twitch"""
        url = 'https://id.twitch.tv/oauth2/token'
        params = {'client_id': self.config.twitch_id,
                  'client_secret': self.config.twitch_secret,
                  'grant_type': 'client_credentials'}
        async with self.aiohttp_session.post(url, params=params) as resp:
            token = (await resp.json())['access_token']
        await self.set_oauth_token(token)
        self.logger.info('Successfully refreshed oauth token.')

    async def request(self, endpoint: str, params: dict[str, str] = {}) -> dict:
        while True:
            headers = {'Authorization': f'Bearer {self.token}', 'Client-ID': self.config.twitch_id}
            async with self.aiohttp_session.get(f'{self.base_url}/{endpoint}',
                                                params=params,
                                                headers=headers) as resp:
                if resp.status == 401:
                    # oauth token is invalid
                    await self.refresh_oauth_token()
                    continue
                elif resp.status == 429:
                    # we are getting ratelimited
                    reset_time = int(resp.headers['Ratelimit-Reset'])
                    await asyncio.sleep(reset_time - time.time())
                return await resp.json()

    async def get_stream(self, user_login: str) -> Optional[TwitchStream]:
        resp = await self.request('streams', {'user_login': user_login})
        if len(resp['data']) == 0:
            return None
        return TwitchStream(**resp['data'][0])

    async def get_user(self, user_id: str) -> TwitchUser:
        resp = await self.request('users', {'id': user_id})
        return TwitchUser(**resp['data'][0])

    async def get_game(self, game_id: str):
        resp = await self.request('games', {'id': game_id})
        return TwitchGame(**resp['data'][0])


# check
def is_admin(ctx: commands.Context):
    """
    Check whether the author has permission to add and remove people from the
    watchlist
    """
    roles = [role.id for role in ctx.author.roles]
    return ctx.bot.config.ids.admin_role in roles or ctx.bot.owner_id == ctx.author.id


class TwitchCog(commands.Cog):
    """
    Cog to handle twitch notifications

    Checks for the current status of given twitch steams, and if a channel
    goes live it creates a fancy embed in a predefined channel.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.session = db.async_sessionmaker
        self.twitch = Twitch(bot)
        self.update_streamers_idx = 0
        self.update_streamers_task.start()

    async def cog_unload(self):
        self.update_streamers_task.cancel()

    async def get_streamer_named(self, streamer_name: str) -> Optional[Streamer]:
        stmt = select(Streamer).where(Streamer.name == streamer_name)
        async with self.session() as session:
            return (await session.execute(stmt)).scalars().first()

    async def monitor_streamer(self, streamer_name: str) -> bool:
        """
        Add a twitch streamer to the list of monitored streamers

        Parameters
        ----------
        streamer_name : str
            The name of the twitch streamer you want to monitor

        Returns
        -------
        bool
            False if the streamer was already monitored, else True
        """
        streamer = await self.get_streamer_named(streamer_name)
        if streamer is not None:
            return False
        streamer = Streamer(name=streamer_name, is_live=False)
        async with self.session() as session:
            async with session.begin():
                session.add(streamer)
        return True

    async def stop_monitoring_streamer(self, streamer_name: str) -> bool:
        streamer = await self.get_streamer_named(streamer_name)
        if streamer is None:
            return False
        async with self.session() as session:
            async with session.begin():
                await session.delete(streamer)
        return True

    async def get_monitored_streamers(self) -> list[Streamer]:
        stmt = select(Streamer)
        async with self.session() as session:
            return (await session.execute(stmt)).scalars().all()

    async def set_streamer_offline(self, streamer: Streamer) -> bool:
        status_changed = streamer.is_live is True
        streamer.is_live = False
        async with self.session() as session:
            async with session.begin():
                session.add(streamer)
        return status_changed

    async def set_streamer_online(self, streamer: Streamer) -> bool:
        status_changed = streamer.is_live is False
        streamer.is_live = True
        async with self.session() as session:
            async with session.begin():
                session.add(streamer)
        return status_changed

    async def is_monitored_streamer(self, streamer_name: str) -> bool:
        return (await self.get_streamer_named(streamer_name)) is not None

    async def get_subscription(self,
                               subscriber_id: int,
                               streamer_name: str) -> Optional[Subscription]:
        stmt = select(Subscription).where(Subscription.streamer == streamer_name,
                                           Subscription.subscriber == subscriber_id)
        async with self.session() as session:
            return (await session.execute(stmt)).scalars().first()

    async def subscribe(self, subscriber_id: int, streamer_name: str) -> bool:
        subscription = await self.get_subscription(subscriber_id, streamer_name)
        if subscription is not None:
            return False
        subscription = Subscription(streamer=streamer_name, subscriber=subscriber_id)
        async with self.session() as session:
            async with session.begin():
                session.add(subscription)
        return True

    async def unsubscribe(self, subscriber_id: int, streamer_name: str) -> bool:
        subscription = await self.get_subscription(subscriber_id, streamer_name)
        if subscription is None:
            return False
        async with self.session() as session:
            async with session.begin():
                await session.delete(subscription)
        return True

    async def get_subscriptions(self, streamer_name: str) -> list[Subscription]:
        stmt = select(Subscription).where(Subscription.streamer == streamer_name)
        async with self.session() as session:
            return (await session.execute(stmt)).scalars().all()

    async def get_subscribers(self, streamer_name: str) -> list[int]:
        return [x.subscriber for x in (await self.get_subscriptions(streamer_name))]

    @commands.group(name='twitch')
    async def twitch_group(self, ctx: commands.Context):
        """
        Group for twitch commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @twitch_group.command(aliases=["list"])
    async def show_streamers(self, ctx):
        """
        Show the list of all streamers on the watchlist
        """
        monitored_streamers = await self.get_monitored_streamers()
        if len(monitored_streamers) == 0:
            return await ctx.send('Not watching any channel.')
        await ctx.send("```\n" + "\n".join(sorted(streamer.name for streamer in monitored_streamers)) + "\n```")

    @twitch_group.command(name='subscribe')
    async def subscribe_cmd(self, ctx, streamer_name=None):
        """
        Subscribe to a streamer on the watchlist

        If subscribed, you will get a mention every time the streamer goes
        live.

        Parameters
        ----------
        streamer_name : str
            the twitch username of the streamer you want to subscribe to

        Example
        -------
        >>> !twitch subscribe Jabbert
        Adds you to the sublist of Jabbert, so every time Jabbert goes live
        you will get mentioned.
        """
        if streamer_name is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify who you want to subscribe to.")
        streamer_name = streamer_name.lower().strip('" \'')
        try:
            await self.subscribe(ctx.author.id, streamer_name)
        except sqlalchemy.exc.DBAPIError:
            await ctx.send(f"It looks like {streamer_name} was not worthy enough for the kingdom.")
        else:
            await ctx.send(f"You are now subscribed to {streamer_name}")
            self.logger.debug('%s - %d subscribed to %s', ctx.author, ctx.author.id, streamer_name)

    @twitch_group.command(name='unsubscribe')
    async def unsubscribe_cmd(self, ctx, streamer_name=None):
        """
        Unsubscribe from a streamer

        Parameters
        ----------
        streamer_name : str
            the twitch username of the streamer you want to unsubscribe from

        Example
        -------
        >>> !twitch unsubscribe Jabbert
        Removes you from the sublist of Jabbert, and you will no longer get
        mentioned by the bot when he goes live.
        """
        if streamer_name is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify who you want to unsubscribe from.")
        streamer_name = streamer_name.lower().strip('" \'')
        was_subscribed = await self.unsubscribe(ctx.author.id, streamer_name)
        if was_subscribed:
            await ctx.send(f'You are no longer subscribed to {streamer_name}.')
            self.logger.info('%s - %d unsubscribed from %s',
                             ctx.author, ctx.author.id, streamer_name)
        else:
            await ctx.send(f'You are not subscribed to {streamer_name}')

    @twitch_group.command(aliases=["add"])
    @commands.check(is_admin)
    async def add_streamer(self, ctx, streamer_name=None):
        """
        Add a streamer to the twitch watchlist.

        Parameters
        ----------
        streamer_name : str
            the name of the twitch streamer you want to add

        Example
        -------
        >>> !twitch add jabbert
        Adds the twitch user "jabbert" to the watchlist
        """
        if streamer_name is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify a twitchname to add.")
        streamer_name = streamer_name.lower()
        await self.monitor_streamer(streamer_name)
        await ctx.send(f"Successfully added {streamer_name} to the watchlist.")
        self.logger.info('%s - %d added %s to the watchlist',
                         ctx.author, ctx.author.id, streamer_name)

    @twitch_group.command(aliases=["remove", "rem", "del", "delete"])
    @commands.check(is_admin)
    async def del_streamer(self, ctx, streamer_name=None):
        """
        Remove a streamer from the twitch watchlist.

        Parameters
        ----------
        streamer_name : str
            the name of the twitch streamer you want to remove

        Example
        -------
        >>> !twitch delete jabbert
        Removes the twitch user "jabbert" from the watchlist
        """
        if streamer_name is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify who you want to remove.")
        streamer_name = streamer_name.lower()
        await self.stop_monitoring_streamer(streamer_name)
        await ctx.send(f"Successfully removed {streamer_name} from the watchlist.")
        self.logger.info('%s - %d removed %s from the watchlist',
                         ctx.author, ctx.author.id, streamer_name)

    @twitch_group.command(hidden=True)
    @commands.is_owner()
    async def manual_update(self, ctx: commands.Context):
        """
        Manually update the currently live twitch streamers
        """
        async with ctx.channel.typing():
            new_live_channels = await self.get_new_live_channels()
        await ctx.send(new_live_channels)
        await self.announce_new_streamers(new_live_channels)

    async def get_new_live_channels(self) -> list[TwitchStream]:
        """
        Check if any of the channels on the watchlist have gone live since
        last check

        Returns
        -------
        list
            a list with dictionaries in it, containing stream data about new
            live streamers
        """
        new_live_streams = []
        for streamer in await self.get_monitored_streamers():
            try:
                stream = await self.twitch.get_stream(streamer.name)
            except Exception as e:
                self.logger.error('Failed to get stream info for %s: %s', streamer.name, e)
                continue
            if not stream:
                # stream is offline
                await self.set_streamer_offline(streamer)
                continue
            if streamer.is_live:
                # stream is online, and was online before
                continue
            await self.set_streamer_online(streamer)
            new_live_streams.append(stream)
        return new_live_streams

    async def create_stream_embed(self, stream: TwitchStream, user: TwitchUser) -> discord.Embed:
        """
        Create a discord embed for a stream

        Parameters
        ----------
        stream: TwitchStream
            the data about the stream

        Returns
        discord.Embed
            an embed containing all the important stream info
        """
        game = await self.twitch.get_game(stream.game_id)
        game = game.name or 'No game specified.'
        display_name = user.display_name or user.login
        embed = discord.Embed(title=stream.title,
                              url=f"https://twitch.tv/{user.login}",
                              colour=discord.Colour(1).dark_magenta())
        embed.add_field(name="**Played Game**", value=game, inline=True)
        embed.add_field(name="**Viewers**", value=stream.viewer_count, inline=True)
        embed.set_image(url=stream.thumbnail_url.format(width=1280, height=720))
        embed.set_footer(text="Twitch.tv")
        icon_url = user.profile_image_url or None
        embed.set_author(name=display_name,
                         url=f"https://twitch.tv/{user.login}",
                         icon_url=icon_url)
        if user.profile_image_url:
            embed.set_thumbnail(url=user.profile_image_url)
        return embed

    async def announce_new_streamers(self, streamers: list[TwitchStream]) -> None:
        channel = self.bot.get_channel(self.bot.config.ids.stream_channel_id)
        for stream in streamers:
            user = await self.twitch.get_user(stream.user_id)
            embed = await self.create_stream_embed(stream, user)
            msg = (f"Oy citizens of the JabKingdom ! {user.login} is now live on "
                   f"https://www.twitch.tv/{user.login} ! Go check it out :wink:!\n")
            for subscriber in await self.get_subscribers(stream.user_name.lower()):
                msg += f" <@{subscriber}>"
            await channel.send(msg, embed=embed)
        if self.update_streamers_idx % 60 == 0:
            self.logger.debug("Twitch updater running. Online time: %i hours.",
                              self.update_streamers_idx // 60)
        self.update_streamers_idx += 1

    @tasks.loop(seconds=60)
    async def update_streamers_task(self):
        """
        Update all twitch channels on the watch list every minute.
        """
        new_live_channels = await self.get_new_live_channels()
        await self.announce_new_streamers(new_live_channels)

    @update_streamers_task.before_loop
    async def before_update_streamers(self):
        await self.bot.wait_until_ready()

async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(TwitchCog(bot))
