import logging
import os

import discord
from discord.ext import commands
import yaml

from bot import Bot


PATH = "data/reaction_roles/"
FILE_NAME = "reaction_roles.yml"
FILE_PATH = PATH + FILE_NAME


class ReactionRoleCog(commands.Cog):
    """
    Cog for adding roles by reaction

    You can link messages in the reaction_channel to role ids,
    and the bot will automatically assign the linked role to
    everyone that reacts to the message with a predefined reaction.

    Also contains a handy utility to list all roles with their ids,
    or to search for a role by name.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.reaction_message_dict = {}
        self.load_data()

    async def add_message(self, role_name: str) -> discord.Message:
        channel = self.bot.get_channel(self.bot.config.ids.reaction_roles_channel)
        msg = await channel.send(role_name)
        self.logger.info('Created new role msg %s - %d', msg.content, msg.id)
        return msg

    def link_msg_to_role(self, message: discord.Message, role: discord.Role):
        self.reaction_message_dict[str(message.id)] = str(role.id)
        self.save_data()
        self.logger.info('Linked message %s - %d to role %s - %d',
                         message.content,
                         message.id,
                         role.name,
                         role.id)

    @commands.group(aliases=["roles"])
    @commands.has_guild_permissions(manage_roles=True)
    async def role(self, ctx):
        """
        Group for role commands

        Can only be used by admins and the bot owner
        Can also only be used from within the JabKingdom
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)


    @role.command(name='add', aliases=['new'])
    async def cmd_add_role(self, ctx, *, role_name=None):
        """
        Add a role and reactable message to gain that role in #set-your-role

        Parameters
        ----------
        role_name: str
            The name of the role you want to add

        Example
        -------
        >>> !role add Track Mania
        Adds a new role with the name "Track Mania" and a corresponding message
        in the #set-your-role channel, and any user who reacts to the message
        will get the "Track Mania" role.
        """
        if role_name is None:
            await ctx.send_help(ctx.command)
            await ctx.send('You need to specify a role name!')
            return
        role_name = role_name.strip('"')
        try:
            role = await ctx.guild.create_role(name=role_name, mentionable=True)
            msg = await self.add_message(role_name)
            await msg.add_reaction("🤔")
            self.link_msg_to_role(msg, role)
        except Exception as e:
            await ctx.send(f'Operation failed: {e}')


    @role.command(name="link", aliases=["link_message_to_role", "lnk"])
    async def cmd_link_message_to_role(self, ctx, message_id=None, role_id=None):
        """
        Link a mesasge to a role

        Parameters
        ----------
        message_id : int
            the id of the message you want to link
        role_id : int
            the id of the role you want to link

        Example
        -------
        >>> !role link 23450987388 1232432138320999
        Links the message with the id 23450987388 to the role with the id
        1232432138320999
        """
        if message_id is None or role_id is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to specify a message_id and a "
                                  "role_id to link.")
        if not message_id.isdigit():
            await ctx.send_help(ctx.command)
            return await ctx.send("message_id must be a number.")
        if not role_id.isdigit():
            await ctx.send_help(ctx.command)
            return await ctx.send("role_id must be a number.")
        try:
            link_role = discord.utils.get(ctx.guild.roles, id=int(role_id))
        except Exception as e:
            return await ctx.send("Couldn't find the role with the role id "
                                  f"{role_id}.")
        channel = self.bot.get_channel(self.bot.config.ids.reaction_roles_channel)
        try:
            message = await channel.fetch_message(int(message_id))
        except discord.errors.NotFound:
            return await ctx.send("Couldn't find a message with id "
                                  f"{message_id}.")
        self.reaction_message_dict[message_id] = role_id
        await message.add_reaction("🤔")
        self.save_data()
        self.logger.info('Linked message %s - %s to role %s - %s',
                         message.content,
                         message.id,
                         link_role.name,
                         link_role.id)
        return await ctx.send("Successfully linked message "
                              f"`{message.content}` to role "
                              f"`{link_role.name}`.")

    @role.command(aliases=["ids"])
    async def allids(self, ctx):
        """
        Show all roles with their ids.
        """
        roles = ctx.guild.roles
        answer = "```\n"
        for roleobj in roles:
            answer += f"{roleobj.id}\t{roleobj.name}\n"
        answer += "```"
        return await ctx.send(answer)

    @role.command()
    async def search(self, ctx, rolename=None):
        """
        Search for a role containing a specific text

        Parameters
        ----------
        rolename : str
            the text you want to be contained in the word

        Example
        -------
        >>> !role search golf
        Shows you all roles and their ids which have "golf" in their name
        (not case sensitive)
        """
        if rolename is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to pass a text to search for.")
        role_objects = ctx.guild.roles
        found = []
        for i in role_objects:
            if rolename.lower() in i.name.lower():
                found.append((i.name, i.id))
        if len(found) == 0:
            await ctx.send("Couldn't find a role that has this text in it.")
        else:
            answer = "```\n"
            for i in found:
                answer += f"{i[1]}\t{i[0]}\n"
            answer += "```"
            await ctx.send(answer)

    # events

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload):
        """
        Test whether the deleted message was linked to a role

        If so, delete the link from the internal database.
        """
        if str(payload.message_id) in self.reaction_message_dict:
            del(self.reaction_message_dict[str(payload.message_id)])
            self.save_data()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """
        Test whether the reaction was on a message thats linked to a role

        If so, add the role to the user.
        """
        str_message_id = str(payload.message_id)
        if (str_message_id in self.reaction_message_dict
            and payload.user_id != self.bot.user.id):
            server = self.bot.get_guild(self.bot.config.ids.guild_id)
            member = server.get_member(payload.user_id)
            role_id = int(self.reaction_message_dict[str_message_id])
            role = discord.utils.get(server.roles, id=role_id)
            await member.add_roles(role)
            self.logger.info('Added role %s - %s to member %s - %s',
                             role.name,
                             role_id,
                             member,
                             member.id)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        """
        Tests whether the reaction was on a message thats linked to a role

        If so, remove the role from the user.
        """
        str_message_id = str(payload.message_id)
        if (str_message_id in self.reaction_message_dict
            and payload.user_id != self.bot.user.id):
            server = self.bot.get_guild(self.bot.config.ids.guild_id)
            member = server.get_member(payload.user_id)
            role_id = int(self.reaction_message_dict[str_message_id])
            role = discord.utils.get(server.roles, id=role_id)
            await member.remove_roles(role)
            self.logger.info('Removed role %s - %s from member %s - %s',
                             role.name,
                             role_id,
                             member,
                             member.id)

    # internal

    def save_data(self):
        """
        Save the data
        """
        with open(FILE_PATH, "w") as fp:
            yaml.safe_dump(self.reaction_message_dict, fp)

    def load_data(self):
        """
        Load the data
        """
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one.", PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, "r") as fp:
                self.reaction_message_dict = yaml.safe_load(fp)
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one.", FILE_PATH)


async def setup(bot: Bot):
    await bot.add_cog(ReactionRoleCog(bot))
