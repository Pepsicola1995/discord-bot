import logging
from io import StringIO

from discord.ext import commands, tasks
from tabulate import tabulate
import pandas as pd

from bot import Bot


class CoronaUpdateCog(commands.Cog):
    """
    Cog for coronavirus updates

    Shows the latest statistics from worldometer, updated every 4 hours.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.url = 'https://raw.githubusercontent.com/chrislopez24/corona-parser/master/cases.csv'
        self.corona_data = pd.DataFrame()
        self.logger = logging.getLogger(__name__)
        self.update_task.start()

    async def cog_unload(self):
        self.update_task.cancel()

    async def fetch_data(self):
        async with self.bot.aiohttp_session.get(self.url) as resp:
            if resp.status != 200:
                return
            raw_data = await resp.text()
        self.corona_data = pd.read_csv(StringIO(raw_data))
        self.corona_data['Country/Other'] = self.corona_data['Country/Other'].map(lambda x: x.lower())
        self.corona_data.sort_values(by='#', inplace=True)
        # Assume that the values are always sorted by total cases...
        # self.corona_data['Total Cases'] = self.corona_data['Total Cases'].map(lambda x: int(x.replace(',', '')))
        # self.corona_data['New Cases'] = self.corona_data['New Cases'].map(lambda x: int(x.replace(',', '')))
        # self.corona_data['Total Deaths'] = self.corona_data['Total Deaths'].map(lambda x: int(x.replace(',', '')))
        # self.corona_data['New Deaths'] = self.corona_data['New Deaths'].map(lambda x: int(x.replace(',', '')))

    def build_country_msg(self, country):
        country_data = self.corona_data.query(f'`Country/Other` == "{country}"')
        headers = ['Country/Other', 'Total Cases', 'New Cases', 'Total Deaths', 'New Deaths']
        rows = [[country_data[x].values[0] for x in headers]]
        return '```\n' + tabulate(rows, headers=headers) + '\n```'

    def build_top_list(self, num_countries=20):
        highest_countries = self.corona_data.head(num_countries)
        headers = ['Country/Other', 'Total Cases', 'New Cases', 'Total Deaths', 'New Deaths']
        values = {}
        for header in headers:
            values[header] = highest_countries[header].values
        rows = []
        for x in range(num_countries):
            rows.append([values[header][x] for header in headers])
        return '```\n' + tabulate(rows, headers=headers) + '\n```'

    @tasks.loop(minutes=30.0)
    async def update_task(self):
        await self.fetch_data()

    @update_task.before_loop
    async def before_fetching_data(self):
        await self.bot.wait_until_ready()

    @commands.group()
    async def corona(self, ctx):
        """
        Group for corona virus related commands
        """
        if not ctx.invoked_subcommand:
            await ctx.invoke(self.leaderboard)

    @corona.command(aliases=['show', 'search'])
    async def stats(self, ctx, country):
        """
        Look up the current stats for a given country
        """
        country = country.lower()
        if country not in list(self.corona_data['Country/Other']):
            return await ctx.send('Sorry, that country is not in my database :(')
        return await ctx.send(self.build_country_msg(country))

    @corona.command(aliases=['lb', 'sb', 'scoreboard', 'list', 'sadboard'])
    async def leaderboard(self, ctx, amount='20'):
        """
        Get a leaderboard of countries sorted by most cases
        """
        if not amount.isnumeric():
            return await ctx.send('Amount must be a natural number!')
        return await ctx.send(self.build_top_list(int(amount) + 1))


async def setup(bot: Bot):
    await bot.add_cog(CoronaUpdateCog(bot))
