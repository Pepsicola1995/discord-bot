import logging
import os
import psutil
import yaml

from discord.ext import commands

from bot import Bot

PATH = "data/vps/"
FILENAME = "game_servers.yml"
FILEPATH = PATH + FILENAME


class VPSCog(commands.Cog):

    def __init__(self, bot: Bot):
        self.bot = bot
        self.servers = {}
        self.logger = logging.getLogger(__name__)
        self.load_data()

    @commands.group(aliases=['server'])
    async def vps(self, ctx):
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @vps.command()
    async def stats(self, ctx):
        """
        Shows you CPU and GPU statistics of the server.
        """
        async with ctx.channel.typing():
            cpu = psutil.cpu_percent(interval=2)
            mem = psutil.virtual_memory()
            overall_mem_gb = mem.total / 1e9
            used_mem_gb = mem.used / 1e9
        await ctx.send(f'CPU-Load: {cpu:.2f}%\t{used_mem_gb:.2f}GB of '
                       f'{overall_mem_gb:.2f}GB RAM in use.')

    @vps.command(aliases=["add"])
    async def add_server(self, ctx, name, port, *, description):
        """
        Add a gameserver to the list of servers

        Parameters
        ----------
        name: str
            the name of the server you want to add

        port: int
            the port on which you can connect to the server

        description: str (optional)
            a *short* description of the server

        Example
        -------
        >>> !vps add "minecraft vanilla" 1338 JabKingdom minecraft server
        Adds a server with the name `minecraft vanilla` thats running on port
        1338 and has the description `JabKingdom minecraft server`
        """
        if name in self.servers:
            await ctx.send_help(ctx.command)
            return await ctx.send("A gameserver with that name already exists")
        gameserver = {
                "port": port,
                "description": description
                }
        self.servers[name] = gameserver
        self.save_data()
        self.logger.info("%s - %i added server %s to the list of gameservers.",
                         ctx.author,
                         ctx.author.id,
                         name)
        return await ctx.send(f"Successfully added {name} to the list of "
                              "gameservers.")

    @vps.command(aliases=["remove", "delete", "rm", "del"])
    async def remove_server(self, ctx, *, name):
        """
        Remove a gameserver from the list of servers

        Parameters
        ----------
        name: str
            the name of the server you want to add

        Example
        -------
        >>> !vps remove "minecraft vanilla"
        Removes the server with the name `minecraft vanilla` if it exists.
        """
        if name not in self.servers:
            await ctx.send_help(ctx.command)
            return await ctx.send("That server does not exist.")
        del(self.servers[name])
        self.save_data()
        self.logger.info("%s - %i removed server %s from the list of gameservers.",
                         ctx.author,
                         ctx.author.id,
                         name)
        return await ctx.send(f"Successfully removed {name} from the list of "
                              "gameservers.")

    @vps.command(aliases=["list"])
    async def list_servers(self, ctx):
        """List all gameservers running on the VPS"""
        ret = ("**List of services on the VPS**\n```\nDomain - IP:\t"
               f"{self.bot.config.vps_domain} - {self.bot.config.vps_ip}\n\n")
        for name, server in self.servers.items():
            ret += (f"Name: {name}\tPort: {server['port']}\n\tDescription: "
                    f"{server['description']}\n\n")
        ret += "```"
        return await ctx.send(ret)

    def save_data(self):
        """Save the data"""
        with open(FILEPATH, "w") as fp:
            yaml.safe_dump(self.servers, fp)
        self.logger.debug("Saved %s", FILEPATH)

    def load_data(self):
        """Load the data"""
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one.", PATH)
        if os.path.isfile(FILEPATH):
            with open(FILEPATH, "r") as fp:
                self.servers = yaml.safe_load(fp)
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one.", FILEPATH)


async def setup(bot: Bot):
    await bot.add_cog(VPSCog(bot))
