"""
MIT License

Copyright (c) 2021 Gina Muuss

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Heavily inspired by https://github.com/GinaMuuss/BasisGradeChangeMailExtension
"""
import re
import os
import logging
import traceback
from collections import defaultdict

import yaml
from aiohttp import ClientSession
from bs4 import BeautifulSoup, Tag
from discord.ext import commands, tasks
from tabulate import tabulate

from bot import Bot

PATH = 'data/basis/'
NAME = 'grades.json'
FILE_PATH = PATH + NAME


class GradeUpdater(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.grades = []
        self.logger = logging.getLogger(__name__)
        self.load_data()
        self.update_task.start()
        self.grade_table_num = 1

    async def cog_unload(self):
        self.update_task.cancel()

    def load_data(self) -> None:
        if not os.path.isdir(PATH):
            self.logger.warning("Couldn't find %s directory, created new one.", PATH)
            os.makedirs(PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, 'r', encoding='utf-8') as fp:
                self.grades = yaml.safe_load(fp)
        else:
            self.logger.warning("Couldn't find %s, created new one.", FILE_PATH)
            self.save_data()

    def save_data(self) -> None:
        with open(FILE_PATH, 'w', encoding='utf-8') as fp:
            yaml.safe_dump(self.grades, fp)

    async def login(self, session: ClientSession) -> str:
        params = {
            'state': 'user',
            'type': '1',
            'category': 'auth.login',
            're': 'last',
            'startpage': 'portal.vm'
        }

        data = {
            'username': os.environ['BASIS_USER'],
            'password': os.environ['BASIS_PW'],
            'submit': 'Anmelden'
        }

        async with session.post('https://basis.uni-bonn.de/qisserver/rds',
                                params=params,
                                data=data) as response:
            text = await response.text()
        soup = BeautifulSoup(text, 'html.parser')
        aas = soup.find("a", text="Notenspiegel")
        return aas.attrs["href"]

    async def extract_grade_page_link(self, session: ClientSession, notenlink: str) -> list[str]:
        async with session.get(notenlink) as response:
            soup = BeautifulSoup(await response.text(), 'html.parser')
        imgs = soup.find_all("img", src="/QIS/images//information.svg")
        return [x.parent.attrs["href"] for x in imgs]

    async def extract_grade_table(self, session: ClientSession, grade_link: str) -> Tag:
        async with session.get(grade_link) as response:
            text = await response.text()
        soup = BeautifulSoup(text, 'html.parser')
        table = soup.find_all("table")[1]
        return table

    def extract_grades(self, table: Tag) -> list[tuple[str, str, str]]:
        rows = []
        for row in table.find_all("tr"):
            tds_text = [td.text.strip() for td in row.find_all("td")]
            if len(tds_text) >= 2 and (
                    'Prüfung' in tds_text[1] or
                    'Modul' in tds_text[1] or
                    tds_text[0].strip() in ('8000', '8001')
            ):
                rows.append((tds_text[0].strip(), tds_text[1], tds_text[3]))
        return rows

    def merge_grades_to_table(self, rows: list[tuple[str, str, str]]) -> str:
        modules = defaultdict(dict)
        for examination_num, name, grade in rows:
            if examination_num in ('8000', '8001'):
                modnr = examination_num
                modules[modnr]['name'] = name
                modules[modnr]['grade'] = grade
            else:
                modnr = re.findall(r'\d\d\d\d', name)[0]
                if 'Modul' in name:
                    modules[modnr]['name'] = name.split('-')[2].strip()
                elif 'Prüfung' in name:
                    modules[modnr]['grade'] = grade

        rows = [(modnr, i['name'], i['grade']) for modnr, i in modules.items()]
        headers = ('Modulnummer', 'Modulname', 'Note')
        return tabulate(rows, headers=headers)

    async def send_grades(self, grade_table: str) -> None:
        msg = f'New grades for you: \n```\n{grade_table}\n```'
        user = (await self.bot.application_info()).owner
        await user.send(msg)

    async def get_grades_from_table(self,
                                    session: ClientSession,
                                    grade_table_num: int,
                                    notenlink: str) -> str:
        grade_links = await self.extract_grade_page_link(session, notenlink)
        table = await self.extract_grade_table(session, grade_links[grade_table_num])
        grade_rows = self.extract_grades(table)
        merged_grades = self.merge_grades_to_table(grade_rows)
        return merged_grades

    async def get_grades(self) -> str:
        async with ClientSession() as session:
            notenlink = await self.login(session)
            grades = await self.get_grades_from_table(session, self.grade_table_num, notenlink)
        return grades

    @commands.command(name='grades', hidden=True)
    @commands.is_owner()
    async def cmd_grades(self, ctx: commands.Context):
        grades = await self.get_grades()
        await self.send_grades(grades)

    @tasks.loop(minutes=30)
    async def update_task(self):
        try:
            self.logger.info('Checking for new grades...')
            grades = await self.get_grades()
            if self.grades != grades:
                self.logger.info('New grades found! Sending...')
                await self.send_grades(grades)
                self.grades = grades
                self.save_data()
        except Exception:
            user = (await self.bot.application_info()).owner
            await user.send(f'Exception for getting grades!\n```py\n{traceback.format_exc()}\n```')


async def setup(bot: Bot):
    await bot.add_cog(GradeUpdater(bot))
