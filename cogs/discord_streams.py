import logging
import time
from typing import Optional

import discord
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.discord_streams import Base
from db.discord_streams import Following
from db.discord_streams import Streamer
from db.discord_streams import Subscription

TIMEOUT_SECONDS = 180


class DiscordStreamCog(commands.Cog):
    """
    Manage notifications for people streaming (games) in discord voice channels.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def cog_unload(self):
        self.logger.info('Unloading cog - closing DB session.')

    async def get_streamer(self, session: AsyncSession, streamer_id: int) -> Optional[Streamer]:
        stmt = select(Streamer).where(Streamer.id == streamer_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def streamer_seen(self, session: AsyncSession, streamer_id: int) -> Streamer:
        """Set the timestamp for when the streamer was last seen to now"""
        streamer = await self.get_streamer(session, streamer_id)
        async with session.begin():
            if streamer is None:
                streamer = Streamer(id=streamer_id,
                                    status=None,
                                    last_status_update=0,
                                    is_live=False,
                                    last_seen=time.time())
            streamer.last_seen = time.time()
            session.add(streamer)
        return streamer

    async def get_subscription(self,
                               session: AsyncSession,
                               subscriber_id: int,
                               streamer_id: int) -> Optional[Subscription]:
        stmt = select(Subscription).where(Subscription.streamer == streamer_id,
                                          Subscription.subscriber == subscriber_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def subscribe(self, session: AsyncSession, subscriber_id: int, streamer_id: int) -> bool:
        if await self.get_subscription(session, subscriber_id, streamer_id) is not None:
            return False
        subscription = Subscription(streamer=streamer_id, subscriber=subscriber_id)
        async with session.begin():
            session.add(subscription)
        return True

    async def unsubscribe(self, session: AsyncSession, subscriber_id: int, streamer_id: int) -> bool:
        if (subscription := await self.get_subscription(session, subscriber_id, streamer_id)) is None:
            return False
        async with session.begin():
            await session.delete(subscription)
        return True

    async def get_subscriptions(self, session: AsyncSession, subscriber_id: int) -> list[int]:
        stmt = select(Subscription.streamer).where(Subscription.subscriber == subscriber_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_subscribers(self, session: AsyncSession, streamer_id: int) -> list[int]:
        stmt = select(Subscription.subscriber).where(Subscription.streamer == streamer_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_follow(self,
                         session: AsyncSession,
                         follower_id: int,
                         game: str) -> Optional[Following]:
        stmt = select(Following).where(Following.follower == follower_id, Following.game == game)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def follow(self, session: AsyncSession, follower_id: int, game: str) -> bool:
        follow = await self.get_follow(session, follower_id, game)
        if follow is not None:
            return False
        follow = Following(game=game, follower=follower_id)
        async with session.begin():
            session.add(follow)
        return True

    async def unfollow(self, session: AsyncSession, follower_id: int, game: str) -> bool:
        follow = await self.get_follow(session, follower_id, game)
        if follow is None:
            return False
        async with session.begin():
            await session.delete(follow)
        return True

    async def get_followers(self, session: AsyncSession, game: str) -> list[int]:
        async with session.begin():
            follows = (await session.execute(select(Following))).scalars().all()
        return [follow.follower for follow in follows if follow.game.lower() in game.lower()]

    async def get_follows(self, session: AsyncSession, follower_id: int) -> list[str]:
        stmt = select(Following.game).where(Following.follower == follower_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def set_status(self, session: AsyncSession, streamer_id: int, game: Optional[str]) -> None:
        stmt = select(Streamer).where(Streamer.id == streamer_id)
        async with session.begin():
            streamer = (await session.execute(stmt)).scalars().first()
        if streamer is None:
            streamer = Streamer(id=streamer_id,
                                status=game,
                                last_status_update=time.time(),
                                is_live=False,
                                last_seen=0)
        else:
            streamer.status = game
            streamer.last_status_update = time.time()
        async with session.begin():
            session.add(streamer)

    @commands.group(name='discord')
    async def discord_group(self, ctx: commands.Context):
        """
        Group for discord commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    async def member_id_from_args(self,
                                  ctx: commands.Context,
                                  streamer_id: Optional[str]) -> Optional[int]:
        if ctx.message.mentions:
            return ctx.message.mentions[0].id
        try:
            return int(streamer_id)
        except ValueError:
            return None

    @discord_group.command(name='subscribe', aliases=['sub'])
    async def subscribe_cmd(self, ctx, streamer_id=None):
        """
        Subscribe to someones discord screenshares

        Parameters
        ----------
        streamer_id : int | str
            the discord id of the person you want to subscribe, or a mention of the user

        Example
        -------
        >>> !discord subscribe 1232432138320999
        Everytime the member with the id 1232432138320999 shares their screen
        in a voice channel, you will get mentioned in #streams
        """
        streamer_id = await self.member_id_from_args(ctx, streamer_id)
        streamer = ctx.guild.get_member(streamer_id)
        async with db.async_sessionmaker() as session:
            new_subscription = await self.subscribe(session, ctx.author.id, streamer_id)
        if new_subscription:
            self.logger.info('%s - %d is now subscribed to %s - %s screenshares',
                             ctx.author, ctx.author.id, streamer, streamer_id)
            await ctx.send(f'Successfully subscribed to {streamer}')
        else:
            await ctx.send(f'You are already subscribed to {streamer}')

    @discord_group.command(name='unsubscribe', aliases=['unsub'])
    async def unsubscribe_cmd(self, ctx, streamer_id=None):
        """
        Unsubscribe from someones discord screenshares

        Parameters
        ----------
        streamer_id : int | str
            the discord id of the person you want to subscribe, or a mention of the user

        Example
        -------
        >>> !discord unsubscribe 1232432138320999
        You will no longer get mentions when the user with the id 1232432138320999
        starts sharing their screen.
        """
        streamer_id = await self.member_id_from_args(ctx, streamer_id)
        streamer = ctx.guild.get_member(streamer_id)
        async with db.async_sessionmaker() as session:
            change = await self.unsubscribe(session, ctx.author.id, streamer_id)
        if change:
            self.logger.info('%s - %d is no longer subscribed to %s - %s screenshares',
                             ctx.author, ctx.author.id, streamer, streamer_id)
            await ctx.send(f'Successfully unsubscribed from user with ID {streamer_id}.')
        else:
            await ctx.send('You are not subscribed to this user.')

    @discord_group.command(name='subscriptions')
    async def subscriptions_cmd(self, ctx):
        """Shows who you are subscribed to for discord screenshares"""
        async with db.async_sessionmaker() as session:
            sub_ids = await self.get_subscriptions(session, ctx.author.id)
        if not sub_ids:
            return await ctx.send('You are not subscribed to anyone.')
        member_names = [f'{ctx.guild.get_member(discord_id)} - {discord_id}'
                        for discord_id in sub_ids]
        members = '\n'.join(member_names)
        return await ctx.send(f'You are subscribed to the following people: ```\n{members}\n```')

    @discord_group.command(name='follow')
    async def follow_cmd(self, ctx, *, game=None):
        """
        Follow a game for a discord screenshare

        Parameters
        ----------
        game : str
            (part of) the name of the game you want to follow

        Example
        -------
        >>> !discord follow rocket
        Everytime a screenshare of a game with a name that includes 'rocket'
        (case insensitive) starts, you will receive a mention.
        """
        if game is None:
            await ctx.send_help(ctx.command)
            return await ctx.send('You need to specify a game name.')
        game = game.strip().strip('"').strip("'")
        async with db.async_sessionmaker() as session:
            await self.follow(session, ctx.author.id, game)
        self.logger.info('%s - %d is now following games with %s in the name',
                         ctx.author, ctx.author.id, game)
        return await ctx.send(f'You are now following all games with `{game}` in their name.')

    @discord_group.command(name='unfollow')
    async def unfollow_cmd(self, ctx, *, game=None):
        """
        Unfollow a game for a discord screenshare

        Parameters
        ----------
        game : str
            (part of) the name of the game you want to unfollow

        Example
        -------
        >>> !discord unfollow rocket
        You will no longer receive mentions of games being screenshared that
        include 'rocket' in their name
        """
        if game is None:
            await ctx.send_help(ctx.command)
            return await ctx.send('You need to specify a game name.')
        game = game.strip().strip('"').strip("'")
        async with db.async_sessionmaker() as session:
            await self.unfollow(session, ctx.author.id, game)
        self.logger.info('%s - %d is no longer following games with %s in the name',
                         ctx.author, ctx.author.id, game)
        return await ctx.send(f'You are no longer following `{game}`')

    @discord_group.command(name='follows')
    async def follows_cmd(self, ctx):
        """Shows which games you are following to for discord screenshares"""
        async with db.async_sessionmaker() as session:
            followed_games = await self.get_follows(session, ctx.author.id)
        if len(followed_games) == 0:
            return await ctx.send('You are not following any games.')
        follow_list = '\n'.join(followed_games)
        return await ctx.send(f'You are following the following games: ```\n{follow_list}\n```')

    @discord_group.command(aliases=['game'])
    async def status(self, ctx, *, status=''):
        """
        Set your status for the next stream.
        The current status will count up to 15 min before the stream

        Alias: !setgame

        Parameters
        ----------
        game : str
            The game you want to set your status to

        Example
        -------
        >>> !discord status Pokémon
        For the next 15 minutes, all your stream announcemens will show that you play Pokémon.
        """
        status = status.strip().strip('"')
        async with db.async_sessionmaker() as session:
            await self.set_status(session, ctx.author.id, status)
        return await ctx.send(f'Successfully set your status to `{status}`')

    @discord_group.command(name='clear_status')
    async def clear_status_cmd(self, ctx):
        """Remove your custom stream status"""
        async with db.async_sessionmaker() as session:
            await self.set_status(session, ctx.author.id, None)
        return await ctx.send('Successfully removed your status.')

    @commands.command(help=status.help, hidden=True)
    async def setgame(self, ctx, *, status=None):
        await ctx.invoke(self.status, status=status)

    async def announcement_mentions(self, session: AsyncSession, streamer_id, game):
        mentions = ['\n']
        for subscriber in await self.get_subscribers(session, streamer_id):
            mentions.append(f'<@{subscriber}>')
        for follower in await self.get_followers(session, game):
            mentions.append(f'<@{follower}>')
        return ''.join(mentions)

    def infer_game(self, streamer: Streamer, member: discord.Member) -> str:
        if streamer.status and streamer.last_status_update > time.time() - (15 * 60):
            # custom game, if one was set less than 15 minutes ago
            return streamer.status
        if member.activity is not None and member.activity.type == discord.ActivityType.playing:
            # else try to get game through discord activity
            return member.activity.name
        return 'A Secret Game'

    def create_stream_embed(self,
                            game: str,
                            member: discord.Member,
                            channel_name: str) -> discord.Embed:
        embed = discord.Embed(title=f'{member.display_name} plays {game}', color=member.color)
        embed.add_field(name='**Played Game**', value=game, inline=True)
        embed.add_field(name='**Channel**', value=channel_name)
        embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
        embed.set_thumbnail(url=member.display_avatar.url)
        return embed

    async def announce_stream(self,
                              session: AsyncSession,
                              streamer: Streamer,
                              member: discord.Member,
                              channel_name: str) -> None:
        game = self.infer_game(streamer, member)
        embed = self.create_stream_embed(game, member, channel_name)
        msg = (f'Oy citizens of the JabKingdom ! **{member.display_name}** is now live in '
               f'`{channel_name}` with **{game}**! Go check it out :wink:!')
        msg += await self.announcement_mentions(session, member.id, game)
        notification_channel = self.bot.get_channel(self.bot.config.ids.stream_channel_id)
        await notification_channel.send(msg, embed=embed)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        if member.guild.id != self.bot.config.ids.guild_id:
            return
        async with db.async_sessionmaker() as session:
            if before.self_stream is False and after.self_stream is True:
                # stream has started
                streamer = await self.get_streamer(session, member.id)
                if streamer and time.time() - streamer.last_seen < TIMEOUT_SECONDS:
                    return
                streamer = await self.streamer_seen(session, member.id)
                await self.announce_stream(session, streamer, member, after.channel.name)
            elif before.self_stream is True and after.self_stream is False:
                # stream has ended
                await self.streamer_seen(session, member.id)


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(DiscordStreamCog(bot))
