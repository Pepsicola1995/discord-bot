import logging

from discord import Guild, Emoji, TextChannel
from discord.ext import commands

from bot import Bot


class EmojiUpdateCog(commands.Cog):
    """
    Cog to notify about emoji updates
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger: logging.Logger = logging.getLogger(__name__)

    @commands.Cog.listener()
    async def on_guild_emojis_update(self, guild: Guild, before: list[Emoji], after: list[Emoji]):
        """
        Send a message when the emojis are updated
        """
        async def send_update(emojis: list[str], channel: TextChannel, msg: str):
            if len(emojis) == 0:
                return
            for emoji in emojis:
                msg += f'- `{emoji}`'
            self.logger.info(msg)
            await channel.send(msg)

        if guild.id != self.bot.config.ids.guild_id:
            return

        renamed = [f'{before[before.index(e)].name} -> {e.name}'
                   for e in after
                   if e in before and e.name != before[before.index(e)].name]
        removed = [e.name for e in set(before) - set(after)]
        added = [e.name for e in set(after) - set(before)]
        channel = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        await send_update(added, channel, msg='New emotes have arrived!\n')
        await send_update(removed, channel, msg='These emotes are not worthy of the kingdom:\n')
        await send_update(renamed, channel, msg='These emotes names were refined:\n')


async def setup(bot: Bot):
    await bot.add_cog(EmojiUpdateCog(bot))
