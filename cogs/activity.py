import asyncio
import datetime
import logging

from discord.ext import commands, tasks
import discord
from tabulate import tabulate
from sqlalchemy import func
from sqlalchemy import Column
from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.activity import Base
from db.activity import Month
from db.activity import Normie
from db.activity import Record


class ActivityTracker(commands.Cog):
    """
    Tracks users activity by channel.

    It tracks word and messagecount per channel,
    directly associated with their discord ids.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.normies = self.load_normies()
        self.month = self.load_month()
        self.logger = logging.getLogger(__name__)
        self.activity_reset_task.start()

    async def cog_unload(self):
        self.activity_reset_task.cancel()

    def load_normies(self):
        with Session(db.BLOCKING_ENGINE) as session:
            normies = session.execute(select(Normie)).scalars().all()
            return [normie.member_id for normie in normies]

    def load_month(self):
        with Session(db.BLOCKING_ENGINE) as session:
            result = session.execute(select(Month)).scalars().first()
            if result is None:
                month_number = datetime.datetime.now().month
                session.add(Month(number=month_number))
                session.commit()
            else:
                month_number = result.number
        return month_number

    @commands.group()
    async def activity(self, ctx):
        """Group for activity commands"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    async def remove_data(self, session: AsyncSession, member_id: int) -> bool:
        """
        Remove all data of a user.

        Parameters
        ----------
        member_id: int
            the discord ID of the user who's data should be removed

        Returns
        -------
        bool
            True if at least one record was found, False otherwise
        """
        statement = select(Record).where(Record.member_id == member_id)
        async with session.begin():
            records = (await(session.execute(statement))).scalars().all()
        present = False
        async with session.begin():
            for record in records:
                present = True
                await session.delete(record)
        return present

    async def confirm(self, author: discord.User, channel: discord.TextChannel, action: str) -> bool:
        await channel.send(f'Are you sure you want to {action}?\n'
                           f'Say "Yes" to confirm.\n'
                           f'**This may be impossible to undo.**')
        try:
            await self.bot.wait_for(
                'message',
                timeout=15,
                check=lambda msg: msg.author == author and msg.channel == channel and msg.content.strip() == 'Yes'
            )
        except asyncio.TimeoutError:
            return False
        return True

    @activity.command(aliases=['optout'])
    async def opt_out(self, ctx):
        """Opt out of activity tracking, and reset all your stats. THIS IS PERMANENT!"""
        if not self.confirm(ctx.author, ctx.channel, 'opt out of activity tracking and delete all your activity data'):
            await ctx.send('Cancelled opt-out and deletion of your activity data')
            return
        statement = select(Normie).where(Normie.member_id == ctx.author.id)
        async with db.async_sessionmaker() as session:
            async with session.begin():
                normie_record = (await session.execute(statement)).scalars().first()
            if normie_record is not None:
                return await ctx.send('You have already opted out, have a nice day :)')
            normie_record = Normie(member_id=ctx.author.id)
            async with session.begin():
                session.add(normie_record)
            await self.remove_data(session, ctx.author.id)
        self.normies.append(ctx.author.id)
        await ctx.send('Successfully opted out of data collection and reset all stats.')

    @activity.command(aliases=['optin'])
    async def opt_in(self, ctx):
        """Opt in to activity tracking."""
        statement = select(Normie).where(Normie.member_id == ctx.author.id)
        async with db.async_sessionmaker() as session:
            async with session.begin():
                normie_record = (await session.execute(statement)).scalars().first()
            if normie_record is None:
                return await ctx.send('You are already opted in for activity tracking.')
            async with session.begin():
                await session.delete(normie_record)
        self.normies.remove(ctx.author.id)
        await ctx.send('Successfully opted back in to data collection.')

    async def get_username(self, discord_id: int, guild: discord.Guild) -> str:
        user = guild.get_member(discord_id)
        if user is None:
            user = self.bot.get_user(discord_id)
        if user is None:
            return f'Unknown user {discord_id}'
        return f'{user.display_name}#{user.discriminator}'

    async def create_global_leaderboard(self, session: AsyncSession) -> str:
        return await self.create_leaderboard(session,
                                             Record.message_count,
                                             Record.word_count)

    async def create_monthly_leaderboard(self, session: AsyncSession) -> str:
        return await self.create_leaderboard(session,
                                             Record.message_count_monthly,
                                             Record.word_count_monthly)

    async def create_leaderboard(self,
                                 session: AsyncSession,
                                 msg_count: Column,
                                 word_count: Column) -> str:
        msg_sum = func.sum(msg_count)
        word_sum = func.sum(word_count)
        stmt = select(Record.member_id, msg_sum.label('msg_count'), word_sum.label('word_count')) \
            .filter(msg_count > 0) \
            .group_by(Record.member_id) \
            .order_by(msg_sum.desc()) \
            .limit(10)
        async with session.begin():
            results = (await session.execute(stmt)).all()
        if len(results) == 0:
            return 'I did not see anyone chatting this month.'
        headers = ['Username', 'Words', 'Messages', 'Words/Message']
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        rows = [[await self.get_username(result.member_id, guild),
                 result.word_count,
                 result.msg_count,
                 result.word_count / result.msg_count] for result in results]
        return tabulate(rows, headers=headers, floatfmt='.2f')

    @activity.command(aliases=["lb", "sb", "scoreboard"])
    async def leaderboard(self, ctx, globally=False):
        """
        Show the top 10 most active people by message count

        Also display their total words/messages sent during the tracking period

        Parameters
        ----------
        globally: bool
            boolean value to determine whether you want to view global data or just
            a limited timeframe.
            accepts boolean-like strings like ('yes', 'no'), ('True', 'False') etc.

        Example
        -------
        >>> !activity leaderboard no
        Shows the top 10 people who sent the most messages in the current month [CE(S)T]
        """
        # TODO: implement blacklist
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                if globally:
                    lb = await self.create_global_leaderboard(session)
                else:
                    lb = await self.create_monthly_leaderboard(session)
        await ctx.send(f'```\n{lb}\n```')

    async def get_channel_or_thread_name(self, discord_id: int) -> str:
        channel_or_thread = self.bot.get_channel(discord_id)
        if channel_or_thread is None:
            guild = self.bot.get_guild(self.bot.config.ids.guild_id)
            try:
                channel_or_thread = await guild.fetch_channel(discord_id)
            except discord.NotFound:
                pass
        if channel_or_thread is None:
            return f'#{discord_id}#'
        return channel_or_thread.name[:37]

    async def create_global_personal_leaderboard(self,
                                                 session: AsyncSession,
                                                 member_id: int) -> str:
        word_count = Record.word_count
        message_count = Record.message_count
        return await self.create_personal_leaderboard(session, member_id, word_count, message_count)

    async def create_monthly_personal_leaderboard(self,
                                                  session: AsyncSession,
                                                  member_id: int) -> str:
        word_count = Record.word_count_monthly
        message_count = Record.message_count_monthly
        return await self.create_personal_leaderboard(session, member_id, word_count, message_count)

    async def create_personal_leaderboard(self,
                                          session: AsyncSession,
                                          member_id: int,
                                          word_count,
                                          message_count) -> str:
        stmt = select(Record.member_id,
                      Record.channel_id,
                      word_count.label('word_count'),
                      message_count.label('msg_count')) \
            .filter(Record.member_id == member_id, message_count > 0) \
            .order_by(message_count.desc())
        results = (await session.execute(stmt)).all()
        if len(results) == 0:
            return 'I did not see you chat this month'
        msg_sum = sum(result.msg_count for result in results)
        word_sum = sum(result.word_count for result in results)
        headline = (f'**Total words**: {word_sum}\t**Total messages**: {msg_sum}\t'
                    f'**words/message:** {word_sum/msg_sum:.2f}')
        rows = [(await self.get_channel_or_thread_name(result.channel_id),
                 str(result.word_count),
                 str(result.msg_count)) for result in results]
        headers = ('Channel Name', 'Words', 'Messages')
        lb = tabulate(rows, headers=headers)
        return f'{headline}\n```\n{lb}\n```'

    @activity.command(aliases=['clb'])
    async def channel_leaderboard(self, ctx: commands.Context, channel: discord.TextChannel, globally: bool=False):
        msg_count = Record.message_count if globally else Record.message_count_monthly
        word_count  = Record.word_count if globally else Record.word_count_monthly
        stmt = select(Record.member_id, Record.channel_id, word_count.label('word_count'), msg_count.label('message_count')) \
               .where(Record.channel_id == channel.id, msg_count > 0) \
               .order_by(msg_count.desc())
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                results = (await session.execute(stmt)).all()
            headers = ['Username', 'Words', 'Messages', 'Words/Message']
            guild = self.bot.get_guild(self.bot.config.ids.guild_id)
            rows = [[await self.get_username(result.member_id, guild),
                     result.word_count,
                     result.message_count,
                     result.word_count / result.message_count] for result in results[:10]]
            table = tabulate(rows, headers=headers, floatfmt=".2f")
        if len(results) == 0:
            await ctx.send(f'There has been no activity for {channel.mention} in the specified timeframe.')
            return
        await ctx.send(f'Activity leaderboard for {channel.mention}:\n```\n{table}\n```')


    @activity.command()
    async def me(self, ctx, globally=False):
        """
        Show stats about your own discord activity

        Parameters
        ----------
        globally: bool
            boolean value to determine whether you want to view global data or just
            a limited timeframe.
            accepts boolean-like strings like ('yes', 'no'), ('True', 'False') etc.

        Example
        -------
        >>> !activity me yes
        Shows you stats about your all time activity
        """
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                if globally:
                    lb = await self.create_global_personal_leaderboard(session, ctx.author.id)
                else:
                    lb = await self.create_monthly_personal_leaderboard(session, ctx.author.id)
        if len(lb) > 2000:
            i = 1997
            while lb[i] != '\n':
                i -= 1
            lb1 = lb[:i] + '```'
            lb2 = '```' + lb[i + 1:]
            await ctx.send(lb1)
            await ctx.send(lb2)
        else:
            await ctx.send(lb)

    @activity.command()
    @commands.is_owner()
    async def delete(self, ctx, discordid=None):
        """
        Delete all saved tracking data about a user

        Parameters
        ----------
        discordid: int
            the discordid of the person you want to remove the stats from

        Example
        -------
        >>> !activity delete 1232432139320999
        Deletes all tracking data of the user with the discordid 1232432139320999
        """
        if discordid is None:
            return await ctx.send_help(ctx.command)
        if not discordid.isdigit():
            return await ctx.send('The discord ID has to be an integer')
        async with db.async_sessionmaker() as session:
            present = await self.remove_data(session, int(discordid))
        if present:
            return await ctx.send('Successfully removed all data about this user.')
        return await ctx.send('No data for this user could be found.')

    async def get_record(self, session: AsyncSession, author_id: int, channel_id: int):
        async with session.begin():
            record = (await session.execute(
                select(Record).where(
                    Record.channel_id == channel_id,
                    Record.member_id == author_id
                )
            )).scalars().first()
        if record is None:
            record = Record(member_id=author_id,
                            channel_id=channel_id,
                            word_count=0,
                            message_count=0,
                            word_count_monthly=0,
                            message_count_monthly=0)
        return record

    @activity.command()
    @commands.is_owner()
    async def set_month(self, ctx, number):
        number = int(number)
        async with db.async_sessionmaker() as session:
            await self.set_current_month(session, number)
        await ctx.send(f'Set current month to {self.month}')

    @commands.Cog.listener()
    async def on_message(self, message):
        ctx = await self.bot.get_context(message)
        if message.content.startswith("!") or \
                ctx.author.bot or\
                not ctx.guild or \
                ctx.guild.id != self.bot.config.ids.guild_id or \
                ctx.author.id in self.normies:
            return
        async with db.async_sessionmaker() as session:
            record = await self.get_record(session, ctx.author.id, ctx.channel.id)
            wordcount = len(message.content.strip(" ").split(" "))
            record.message_count += 1
            record.message_count_monthly += 1
            record.word_count += wordcount
            record.word_count_monthly += wordcount
        async with db.async_sessionmaker() as session:
            async with session.begin():
                session.add(record)

    async def reset_monthly_counts(self, session: AsyncSession):
        async with session.begin():
            results: list[Record] = (await session.execute(select(Record))).scalars()
        async with session.begin():
            for result in results:
                result.word_count_monthly = 0
                result.message_count_monthly = 0
                session.add(result)

    async def set_current_month(self, session: AsyncSession, number=None):
        if number is None:
            number = datetime.datetime.now().month
        async with session.begin():
            month = (await session.execute(select(Month))).scalars().first()
            month.number = number
        async with session.begin():
            session.add(month)
        self.month = number

    @tasks.loop(hours=1)
    async def activity_reset_task(self):
        if datetime.datetime.now().month == self.month:
            return
        bot_stuff = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        async with bot_stuff.typing():
            async with db.async_sessionmaker() as session:
                lb = await self.create_monthly_leaderboard(session)
                await bot_stuff.send(f'Resetting monthly data for the new month!\n'
                                     f'This months leaderboard:\n```\n{lb}\n```')
                await self.reset_monthly_counts(session)
                await self.set_current_month(session)

    @activity_reset_task.before_loop
    async def before_activity_reset(self):
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(ActivityTracker(bot))
