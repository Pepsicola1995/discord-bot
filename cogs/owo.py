from __future__ import annotations

import asyncio
import datetime
import logging
import re
import random
from typing import Optional, Iterable
from collections import Counter

import discord
from discord.ext import commands
from discord.ext import tasks
from discord.ext.commands import Cog
from discord.utils import escape_mentions
from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.ext.asyncio import AsyncSession
from tabulate import tabulate

import db
from bot import Bot
from db.one_word_only import Base
from db.one_word_only import Day
from db.one_word_only import Message


def owoify_text(text: str) -> str:
    max_stutters = 3  # 1 = 10%
    min_stutters = 0
    emojis = ['OwO', 'omo', 'umu', 'UwU', '>///<', ':3', '>:3', '~', 'X3']
    # basic owoing
    text = text.replace("R", "W").replace("r", "w")
    text = text.replace("L", "W").replace("l", "w")
    # d-d-did i s-stutter?
    words = text.split(' ')
    word_amount = sum(1 for word in words if word.isalpha() and len(word) > 1)
    stutters = random.randint(min_stutters, max_stutters)
    stutters = int((stutters * 0.1) * word_amount) + 1
    stutters = 0 if word_amount == 0 else stutters
    stutter_pos = []
    while len(stutter_pos) != stutters:
        pos = random.randint(0, len(words) - 1)
        if words[pos].isalpha() and len(words[pos]) > 1 and pos not in stutter_pos:
            stutter_pos.append(pos)
    for x in stutter_pos:
        words[x] = words[x][0] + '-' + words[x]
    emoji = random.choice(emojis)
    if not random.randint(0, 3):
        words = [emoji] + words
    else:
        words += [emoji]
    # blush
    if not random.randint(0, 5):
        pos = random.randint(0, len(words) - 1)
        words = words[:pos] + ['*blushes*'] + words[pos:]
    return ' '.join(words)


class OneWordOnly(Cog):
    def __init__(self, bot: Bot) -> None:
        self.bot = bot
        self.owo_id = self.bot.config.ids.one_word_only_channel
        self.day = self.load_day()
        self.send_daily_owo_gist.start()
        self.logger = logging.getLogger(__name__)
        asyncio.create_task(self.load_missing_messages())

    async def cog_unload(self) -> None:
        self.send_daily_owo_gist.cancel()

    def load_day(self):
        with Session(db.BLOCKING_ENGINE) as session:
            result = session.execute(select(Day)).scalars().first()
            if result is None:
                day = datetime.datetime.now().day
                session.add(Day(day=day))
                session.commit()
            else:
                day = result.day
        return day

    async def load_missing_messages(self) -> None:
        await self.bot.wait_until_ready()
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        try:
            owo_channel = await guild.fetch_channel(self.owo_id)
        except discord.NotFound:
            return
        async with db.async_sessionmaker() as session:
            last_msg = (await session.execute(select(Message).order_by(Message.timestamp.desc()))).scalars().first()
            last_known_msg = last_msg.get_utc_time()
            print(last_known_msg)
            new = 0
            seen = 0
            async for message in owo_channel.history(limit=None, after=last_known_msg):
                new += not await self.edit_message(session, message)
                seen += 1
        print(f'I have added {new}/{seen} new messages.')

    async def get_all_messages(self, session: AsyncSession) -> list[str]:
        stmt = select(Message.content).order_by(Message.timestamp.asc())
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_full_text(self, session: AsyncSession) -> str:
        results = await self.get_all_messages(session)
        story = ' '.join(results)
        for char in '!?,.-':
            story = story.replace(f' {char}', char)
        return story

    async def get_yesterdays_text(self, session: AsyncSession) -> str:
        today = datetime.datetime.now() \
                .astimezone() \
                .replace(hour=0, minute=0, second=0, microsecond=0) \
                .astimezone(datetime.timezone.utc) \
                .replace(tzinfo=None)
        yesterday = today - datetime.timedelta(days=1)
        statement = select(Message) \
                    .where(Message.timestamp < today, Message.timestamp >= yesterday) \
                    .order_by(Message.timestamp.asc())
        async with session.begin():
            results = (await session.execute(statement)).scalars().all()
        return ' '.join([m.content for m in results])

    def paginate(self,
                 msg: str,
                 maxlen: int = 2000,
                 split_chars: Iterable[str] = '!?.') -> list[str]:
        msgs = []
        while len(msg) > maxlen:
            # Find the last occurence of one of the split_chars whose index is before maxlen
            # The char we split by is always the last char of a page/split.
            splits = [-(msg[:maxlen][::-1].find(char)) - (len(msg) - maxlen)
                      for char in split_chars]
            split_pos = max(splits)
            msgs.append(msg[:split_pos])
            msg = msg[split_pos:].strip()
        msgs.append(msg)
        return msgs

    @commands.command()
    @commands.is_owner()
    async def owoyesterday(self, ctx: commands.Context):
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                yesterday_text = await self.get_yesterdays_text(session)
            msgs = self.paginate(yesterday_text)
        for msg in msgs:
            await ctx.send(msg)

    @commands.command()
    @commands.is_owner()
    async def oworeadhist(self, ctx: commands.Context, limit='300'):
        async with ctx.typing():
            limit = int(limit)
            guild = self.bot.get_guild(self.bot.config.ids.guild_id)
            try:
                channel = await guild.fetch_channel(self.owo_id)
            except discord.NotFound:
                self.logger.error('Failed to fetch the owo channel!')
                return
            i = 0
            async with db.async_sessionmaker() as session:
                async for message in channel.history(limit=limit):
                    i += not await self.edit_message(session, message)
        await ctx.send(f'I have added {i} messages.')

    async def set_current_day(self, session: AsyncSession, day=None) -> None:
        if day is None:
            day = datetime.datetime.now().day
        async with session.begin():
            current_day = (await session.execute(select(Day))).scalars().first()
            current_day.day = day
            session.add(current_day)
        self.day = day

    def choose_random_sentence(self, story: str) -> str:
        story = story.replace('<@!', '<@')
        splits = list(re.split(r'(\.|\!|\?)', story))[:-1]
        sentences = []
        for split in splits:
            if not split.strip('.!?"\''):
                sentences[-1] += split
            else:
                sentences.append(split)
        return random.choice(sentences)

    def create_sentence_embed(self, sentence: str) -> str:
        embed = discord.Embed(color=discord.Color.orange(), title='')
        embed.add_field(name='OwO', value=sentence)
        return embed

    @commands.command()
    @commands.is_owner()
    async def owosetday(self, ctx: commands.Context, day):
        if not day.isdigit():
            return await ctx.send('Invalid day!')
        day = int(day)
        async with db.async_sessionmaker() as session:
            await self.set_current_day(session, day)
        await ctx.send('Successfully set current day!')

    @commands.command()
    @commands.is_owner()
    async def owogetday(self, ctx: commands.Context):
        async with db.async_sessionmaker() as session:
            current_day = (await session.execute(select(Day))).scalars().first()
        await ctx.send(current_day.day)

    @commands.cooldown(1, 3600 / 2)
    @commands.command()
    async def owostory(self, ctx: commands.Context):
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                story = await self.get_full_text(session)
            escaped_story = escape_mentions(story)
            pages = self.paginate(escaped_story, 1997)
        await ctx.send(f'**The full <#{self.owo_id}> story:**')
        for msg in pages:
            await ctx.send(f'> {msg}')

    @commands.cooldown(1, 3600 / 2)
    @commands.command()
    async def owostoryowo(self, ctx: commands.Context):
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                story = await self.get_full_text(session)
            escaped_story = escape_mentions(story)
            owo_story = owoify_text(escaped_story)
            pages = self.paginate(owo_story, 1997)
        await ctx.send(f'**The fuww <#{self.owo_id}> stowy:**')
        for msg in pages:
            await ctx.send(f'> {msg}')

    @commands.command()
    async def owosentence(self, ctx: commands.Context):
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                story = await self.get_full_text(session)
            sentence = self.choose_random_sentence(story).strip()
        await ctx.send(embed=self.create_sentence_embed(sentence))

    @commands.command()
    async def owosentenceowo(self, ctx: commands.Context):
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                story = await self.get_full_text(session)
            sentence = self.choose_random_sentence(story).strip()
            owosentence = owoify_text(sentence)
        await ctx.send(embed=self.create_sentence_embed(owosentence))

    @commands.command(aliases=['owolb'])
    async def owoleaderboard(self, ctx: commands.Context, limit='10'):
        if not limit.isdigit():
            return await ctx.send('limit must be a natural number')
        limit = int(limit)
        if not 0 < limit < 51:
            return await ctx.send('limit must be between 1 and 50')
        async with db.async_sessionmaker() as session:
            msgs = [msg.lower().strip(' !?.,') for msg in await self.get_all_messages(session)]
        top_msgs = [(msg, str(msg_count)) for msg, msg_count in Counter(msgs).most_common(limit)]
        await ctx.send('```\n' + tabulate(top_msgs, headers=('Word', 'Count')) + '```')

    async def append_message(self, session: AsyncSession, message: discord.Message):
        msg = Message.from_discord_message(message)
        async with session.begin():
            session.add(msg)

    async def get_message(self, session: AsyncSession, msg_id: int) -> Optional[Message]:
        async with session.begin():
            return (await session.execute(
                select(Message).where(Message.id == msg_id)
            )).scalars().first()

    async def edit_message(self, session: AsyncSession, message: discord.Message) -> bool:
        msg = await self.get_message(session, message.id)
        if msg is None:
            msg = Message.from_discord_message(message)
            existed = False
        else:
            msg.content = message.content
            existed = True
        session.add(msg)
        await session.commit()
        return existed

    async def delete_message(self, session: AsyncSession, msg_id: int) -> bool:
        msg = await self.get_message(session, msg_id)
        if msg is None:
            return False
        async with session.begin():
            await session.delete(msg)
        return True

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.channel.id == self.owo_id:
            async with db.async_sessionmaker() as session:
                await self.append_message(session, message)

    @Cog.listener()
    async def on_message_edit(self, _before: discord.Message, after: discord.Message):
        if after.channel.id == self.owo_id:
            async with db.async_sessionmaker() as session:
                await self.edit_message(session, after)

    @Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        if message.channel.id == self.owo_id:
            async with db.async_sessionmaker() as session:
                await self.delete_message(session, message.id)

    @tasks.loop(minutes=1)
    async def send_daily_owo_gist(self):
        if datetime.datetime.now().day == self.day:
            return
        bot_stuff = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        async with db.async_sessionmaker() as session:
            yesterday_text = await self.get_yesterdays_text(session)
            if yesterday_text.strip() == '':
                await self.set_current_day(session)
                return
            msgs = self.paginate(yesterday_text, 1997)
            await bot_stuff.send('**Yesterdays one-word-only text:**')
            for msg in msgs:
                await bot_stuff.send(f'> {msg}')
            await self.set_current_day(session)

    @send_daily_owo_gist.before_loop
    async def before_send_daily_owo_gist(self):
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(OneWordOnly(bot))
