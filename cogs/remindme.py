import asyncio
import calendar
import datetime
import logging
import time
import math
import traceback
import sys
from typing import Optional

import dateparser
import discord
from discord.ext import tasks
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from tabulate import tabulate

import db
from bot import Bot
from db.locale import Locale
from db.remindme import Base
from db.remindme import Reminder
from db.remindme import Separator
from .utils import paginate


DEFAULT_SEPARATORS = [" that ", " to ", " about "]


def default_separators(user_id: int) -> list[Separator]:
    return [Separator(user_id=user_id, separator=separator) for separator in DEFAULT_SEPARATORS]


class RemindmeCog(commands.Cog):
    """
    Cog for reminders

    Allows users to set "reminders" for any point in the future.
    The bot will DM the user with the provided reminder text at the time
    the user set.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.expand_unit = {
            "h": "hour",
            "hr": "hour",
            "hour": "hour",
            "hours": "hour",
            "m": "minute",
            "min": "minute",
            "minute": "minute",
            "minutes": "minute",
            "d": "day",
            "day": "day",
            "days": "day",
            "mn": "month",
            "month": "month",
            "months": "month",
            "y": "year",
            "yr": "year",
            "year": "year",
            "years": "year"
        }
        self.units = {
            "minute": 1,
            "hour": 60,
            "day": 1440,
            "month": 43200,
            "year": 525600,
        }
        self.update_reminders.start()

    @property
    def basic_parsing_settings(self):
        return {'PREFER_DATES_FROM': 'future', 'RETURN_AS_TIMEZONE_AWARE': True}

    async def cog_unload(self):
        self.update_reminders.cancel()

    def build_timedelta_string(self, td: datetime.timedelta):
        years = td.days // 365
        months = (td.days % 365) // 30
        days = ((td.days % 365) % 30)
        hours = td.seconds // 3600
        minutes = (td.seconds % 3600) // 60
        seconds = ((td.seconds % 3600) % 60)
        ret = ""
        if years:
            ret += f"{years} years, "
        if months:
            ret += f"{months} months, "
        if days:
            ret += f"{days} days, "
        if hours:
            ret += f"{hours} hours, "
        if minutes:
            ret += f"{minutes} minutes, "
        if seconds:
            ret += f"{seconds} seconds, "
        if not ret:
            return ""
        return ret[:-2]

    async def get_expired_reminders(self, session: AsyncSession) -> list[Reminder]:
        naive_utc_now = datetime.datetime.now() \
            .astimezone(datetime.timezone.utc) \
            .replace(tzinfo=None)
        stmt = select(Reminder).where(Reminder.time < naive_utc_now)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_reminders(self,
                            session: AsyncSession,
                            user_id: int,
                            before: datetime.datetime = datetime.datetime.max) -> list[Reminder]:
        stmt = select(Reminder) \
            .where(Reminder.user_id == user_id, Reminder.time < before) \
            .order_by(Reminder.time.asc())
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def add_reminder(self,
                           session: AsyncSession,
                           user_id: int,
                           reminder_time: datetime.datetime,
                           text: str) -> Reminder:
        reminder = Reminder(user_id=user_id, time=reminder_time, text=text)
        async with session.begin():
            session.add(reminder)
        return reminder

    async def remove_reminder(self,
                              session: AsyncSession,
                              user_id: int,
                              reminder_id: int) -> Optional[Reminder]:
        stmt = select(Reminder).where(Reminder.user_id == user_id, Reminder.id == reminder_id)
        async with session.begin():
            reminder = (await session.execute(stmt)).scalars().first()
            if reminder is not None:
                await session.delete(reminder)
        return reminder

    async def purge_reminders(self, session: AsyncSession, user_id: int) -> list[Reminder]:
        stmt = select(Reminder).where(Reminder.user_id == user_id)
        async with session.begin():
            reminders = (await session.execute(stmt)).scalars().all()
            for reminder in reminders:
                await session.delete(reminder)
        return reminders

    async def get_separators(self, session: AsyncSession, user_id: int) -> list[str]:
        stmt = select(Separator.separator).where(Separator.user_id == user_id)
        async with session.begin():
            separators = (await session.execute(stmt)).scalars().all()
        if len(separators) == 0:
            await self.reset_separators(session, user_id)
            return DEFAULT_SEPARATORS
        return separators

    async def add_separator(self, session: AsyncSession, user_id: int, separator: str) -> bool:
        stmt = select(Separator.separator).where(Separator.user_id == user_id,
                                                 Separator.separator == separator)
        async with session.begin():
            if (await session.execute(stmt)).scalars().first() is not None:
                return False
            session.add(Separator(user_id=user_id, separator=separator))
        return True

    async def remove_separator(self, session: AsyncSession, user_id: int, separator: str) -> bool:
        stmt = select(Separator).where(Separator.user_id == user_id,
                                       Separator.separator == separator)
        async with session.begin():
            sep = (await session.execute(stmt)).scalars().first()
            if sep is None:
                return False
            await session.delete(sep)
        return True

    async def reset_separators(self, session: AsyncSession, user_id: int) -> list[str]:
        stmt = select(Separator).where(Separator.user_id == user_id)
        async with session.begin():
            separators = (await session.execute(stmt)).scalars().all()
            for sep in separators:
                await session.delete(sep)
            session.add_all(default_separators(user_id))
        return [s.separator for s in separators]

    def separate_message(self, message: str, separators: list[str]):
        chosen_sep = separators[0]
        first = len(message)
        for sep in separators:
            if sep not in message:
                continue
            i = message.index(sep)
            if i < first:
                first = i
                chosen_sep = sep
        return [x.strip() for x in message.split(chosen_sep, 1)]

    def check_separator_present(self, message: str, separators: list[str]):
        for separator in separators:
            if separator in message:
                return True
        return False

    @commands.command()
    async def reminder(self, ctx, quantity=None, unit=None, *, text=None):
        """
        Remind you via PM of some event.

        Parameters
        ----------
        quantity : int
            the amount of units you want the bot to remind you in
        unit : str
            a time unit. Valid units are: (minutes, hours, days, months, years)
        text : str
            the text you want the bot to remind you of.

        Example
        -------
        >>> !reminder 6 hours drink lots of water
        Sends you a PM in 6 hours with a message containing
        "drink lots of water"
        """
        if quantity is None or unit is None or text is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You are missing arguments!")
        try:
            quantity = float(quantity)
            unit = self.expand_unit[unit]
        except (ValueError, KeyError):
            await ctx.send_help(ctx.command)
            return await ctx.send("Invalid time!")
        if not 0 < quantity < 1024 or math.isnan(quantity):
            return await ctx.send("<quantity> must be a number between 0 and 1024!")

        attachment_urls = [x.url for x in ctx.message.attachments]
        for url in attachment_urls:
            text += f'\n{url} '
        reminder_unix_time = round(time.time() + self.units[unit] * quantity * 60)
        reminder_datetime = datetime.datetime.fromtimestamp(reminder_unix_time) \
            .astimezone(datetime.timezone.utc) \
            .replace(tzinfo=None)
        async with db.async_sessionmaker() as session:
            await self.add_reminder(session, ctx.author.id, reminder_datetime, text)
        if quantity != 1:
            unit += "s"
        return await ctx.send(f"I will remind you of that in {quantity} {unit}")

    async def parse_reminder(self,
                             session: AsyncSession,
                             ctx: commands.Context,
                             msg: Optional[str]) -> tuple[str, str]:
        if msg is None:
            raise TypeError('Missing required parameter msg')
        separators = await self.get_separators(session, ctx.author.id)
        if not self.check_separator_present(msg, separators):
            raise ValueError('You have to separate the reminder time and content by one of your'
                             'separators.')
        time_str, reminder_content = self.separate_message(msg, separators)
        if len(reminder_content) > 1200:
            raise ValueError('Your message text may not be longer than 1200 characters.')
        attachment_urls = [x.url for x in ctx.message.attachments]
        for url in attachment_urls:
            reminder_content += f'\n{url} '

        return time_str, reminder_content

    async def locale_for_user(self, session: AsyncSession, user_id: int) -> Optional[Locale]:
        stmt = select(Locale).where(Locale.id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def locale_str_for_user(self, session: AsyncSession, user_id: int) -> str:
        locale = await self.locale_for_user(session, user_id)
        if locale is None:
            return 'UTC'
        return locale.locale

    async def tzinfo_for_user(self, session: AsyncSession, user_id: int) -> datetime.tzinfo:
        locale = await self.locale_for_user(session, user_id)
        if locale is None:
            return datetime.timezone.utc
        return locale.tzinfo

    async def parsing_settings_for_user(self, session: AsyncSession, user_id: int) -> dict:
        settings = self.basic_parsing_settings
        locale_str = await self.locale_str_for_user(session, user_id)
        settings['TIMEZONE'] = locale_str
        return settings

    @commands.command()
    async def remindme(self, ctx, *, msg=None):
        """
        Remind you via PM of some event.

        Parameters
        ----------
          args: str
            A string in the format `<when> <separator> <what>` where <when> is a
            description *when* you want to be reminded, and <what> is *what*
            you want to be reminded of.
            To see your separators do `!remindconf separators list`

        Example
        -------
        >>> !remindme in 6 hours that you should drink lots of water
        Sends you a PM in 6 hours with a message containing
        "you should drink lots of water"
        """
        async with db.async_sessionmaker() as session:
            time_str, reminder_content = await self.parse_reminder(session, ctx, msg)
            parsing_settings = await self.parsing_settings_for_user(session, ctx.author.id)

        await ctx.channel.typing()
        absolute_datetime = dateparser.parse(time_str, settings=parsing_settings)
        if absolute_datetime is None:
            raise ValueError(f'Sorry, i did not understand the time `{time_str}`\nRemember to not '
                             f'use ambiguous times!')

        absolute_utc_datetime = absolute_datetime. \
                                astimezone(datetime.timezone.utc). \
                                replace(tzinfo=None)
        async with db.async_sessionmaker() as session:
            reminder = await self.add_reminder(session,
                                               ctx.author.id,
                                               absolute_utc_datetime,
                                               reminder_content)

        relative_datetime = absolute_datetime - datetime.datetime.now(tz=absolute_datetime.tzinfo)
        relative_time_str = self.build_timedelta_string(relative_datetime)
        # unixtimestamp in UTC as expected by discord
        unixtime = calendar.timegm(absolute_datetime.utctimetuple())
        warning = ('\n**Your current timezone is UTC. If this is wrong, you can set it with the '
                   '`!locale set` command.**' if parsing_settings['TIMEZONE'] == 'UTC' else '')
        return await ctx.send(f'Created reminder with ID `{reminder.id}` and content:\n'
                              f'```\n{reminder_content}\n```'
                              f'Reminding you in {relative_time_str} on <t:{int(unixtime)}:F>.' + warning)

    @remindme.error
    async def remindme_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, TypeError):
            await ctx.send_help(ctx.command)
            return await ctx.send(str(error))
        if isinstance(error, ValueError):
            await ctx.send_help(ctx.command)
            await ctx.send(str(error))
        else:
            print(f'Ignoring exception in command {ctx.command}:', file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    def reminder_string(self, reminder: Reminder, tzinfo: datetime.tzinfo) -> str:
        return f"[{reminder.id}]\t{reminder.localtime(tzinfo)} :\t{reminder.text}"

    def trim(self, s: str, max_len: int) -> str:
        if len(s) <= max_len:
            return s
        return f'{s[:max_len - 3]}...'

    async def build_reminders_message(self,
                                      author: discord.Member,
                                      reminders: list[Reminder],
                                      tzinfo: datetime.tzinfo):
        rows = [
            (str(reminder.id),
             f'{reminder.localtime(tzinfo)}',
             self.trim(reminder.text, max_len=64))
            for reminder in reminders
        ]
        headers = ('ID', 'Time', 'Content')
        table = tabulate(rows, headers=headers)
        return f'**{author}:**\n```\n{table}\n```'

    @commands.command()
    async def forgetme(self, ctx):
        """
        Remove ALL your reminders.
        """
        def check(msg: discord.Message):
            return msg.author == ctx.author and msg.channel == ctx.channel
        await ctx.channel.send("Are you sure you want to delete ALL your "
                               "reminders? Say 'Yes' to confirm. **This "
                               "will print out all your remaining reminders.**")
        try:
            confirmation = await self.bot.wait_for("message", check=check, timeout=15)
        except asyncio.TimeoutError:
            confirmation = None
        if confirmation is None or confirmation.content.strip().lower() not in ["y", "yes"]:
            return await ctx.send("Canceled deletion of all your reminders.")

        async with db.async_sessionmaker() as session:
            reminders = await self.purge_reminders(session, ctx.author.id)
            user_tzinfo = await self.tzinfo_for_user(session, ctx.author.id)
        await ctx.send(await self.build_reminders_message(ctx.author, reminders, user_tzinfo))
        return await ctx.send("Successfully removed all your reminders.")

    @commands.command(name="forget")
    async def forget_reminders(self, ctx, *reminder_ids):
        """
        Remove a given reminder

        Parameters
        ----------
        reminder_ids: list[int]
            The ids (see !reminders for the numbers) of the reminders you want to delete,
            space separated

        Example
        -------
        >>>!forget 2 3 20
        Deletes the reminders with the ids 2, 3 and 20
        """
        if len(reminder_ids) == 0:
            return await ctx.send_help(ctx.command)
        if not all(x.isdecimal() for x in reminder_ids):
            await ctx.send_help(ctx.command)
            return await ctx.send('The reminder ids have to be integers!')

        reminder_ids = [int(i) for i in reminder_ids]
        removed_reminders = []
        async with db.async_sessionmaker() as session:
            removed_reminders = [await self.remove_reminder(session, ctx.author.id, reminder_id)
                                 for reminder_id in reminder_ids]
            user_tzinfo = await self.tzinfo_for_user(session, ctx.author.id)
        reminders_string = ''.join([f'\n- `{self.reminder_string(r, user_tzinfo)}`'
                                   for r in removed_reminders
                                   if r is not None])
        return await ctx.send(f"Successfully deleted {reminders_string}")

    @commands.command(aliases=["reminders"])
    async def my_reminders(self, ctx):
        """Show all your reminders"""
        async with db.async_sessionmaker() as session:
            reminders = await self.get_reminders(session, ctx.author.id)
            user_tzinfo = await self.tzinfo_for_user(session, ctx.author.id)
        if len(reminders) == 0:
            return await ctx.send('You have no reminders.')
        reminders = await self.build_reminders_message(ctx.author, reminders, user_tzinfo)
        messages = paginate.paginate_codeblock(reminders)
        for message in messages:
            await ctx.send(message)

    # remindme config
    @commands.group()
    async def remindconf(self, ctx):
        """Configure your reminder settings"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @remindconf.group(aliases=["separators"])
    async def separator(self, ctx):
        """Settings for your separators"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @separator.command(name="add")
    async def add_separator_cmd(self, ctx, separator=None):
        """
        Add a new separator

        Parameters
        ----------
        separator: str
            The separator you want to add.

        Example
        -------
        >>> !remindconf separator add " to "
        Adds the separator ` to ` to the list of your separators
        """
        if separator is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to provide a separator!")
        async with db.async_sessionmaker() as session:
            new = await self.add_separator(session, ctx.author.id, separator)
        if not new:
            return await ctx.send("You already added this separator.")
        return await ctx.send(f"Successfully added `{separator}` to your "
                              "separators. Remember that ' ' (spaces) are "
                              "not automatically added around it!")

    @separator.command(name="del", aliases=["delete", "remove", "rm"])
    async def del_separator(self, ctx, separator=None):
        """
        Remove a separator

        Parameters
        ----------
        separator: str
            The separator you want to remove

        Example
        -------
        >>> !remindconf separator remove to
        Removes the separator `to` from the list if your separators
        """
        if separator is None:
            await ctx.send_help(ctx.command)
            return await ctx.send("You need to provide a separator!")
        async with db.async_sessionmaker() as session:
            if len(await self.get_separators(session, ctx.author.id)) <= 1:
                return await ctx.send("You can not remove your last separator.")
            removed = await self.remove_separator(session, ctx.author.id, separator)
        if not removed:
            return await ctx.send("You did not set this separator.")
        return await ctx.send(f"Successfully removed `{separator}` from your separators.")

    @separator.command(name="clear", aliases=["reset"])
    async def clear_separator(self, ctx):
        """Reset your separators to the default set of separators"""
        async with db.async_sessionmaker() as session:
            old_separators = await self.reset_separators(session, ctx.author.id)
        resp = (f"Successfully set your separators to `{DEFAULT_SEPARATORS}`, "
                f"your old separators were `{old_separators}`.")
        return await ctx.send(resp)

    @separator.command(name="list")
    async def list_separators(self, ctx):
        """Show all your current separators"""
        async with db.async_sessionmaker() as session:
            separators = await self.get_separators(session, ctx.author.id)
        return await ctx.send(f"`{separators}`")

    # loops
    @tasks.loop(seconds=30.0)
    async def update_reminders(self):
        """
        Test whether a reminder has to be sent

        Every 30 seconds test whether the time on a reminder has passed
        If so, tell the author of the reminder and then remove the reminder
        from the internal cache
        """
        async with db.async_sessionmaker() as session:
            for reminder in await self.get_expired_reminders(session):
                user = self.bot.get_user(reminder.user_id)
                if user is None:
                    self.logger.error('Tried to remind user %d, but can not find them!',
                                      reminder.user_id)
                else:
                    try:
                        await user.send(f"Hey, don't forget this:\n**{reminder.text}**")
                    except discord.errors.Forbidden as exc:
                        self.logger.error('Could not DM user %s: %s', user, exc)
                    except discord.errors.HTTPException:
                        continue
                async with session.begin():
                    await session.delete(reminder)

    @update_reminders.before_loop
    async def before_update_reminders(self):
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(RemindmeCog(bot))
