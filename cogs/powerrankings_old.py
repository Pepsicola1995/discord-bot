import json
import logging
import os
import random

from discord.ext import commands

from bot import Bot

PATH = 'data/power_rankings'
FILE_NAME = 'power_rankings.json'
FILE_PATH = os.path.join(PATH, FILE_NAME)


def has_permissions(ctx: commands.Context):
    """
    Check whether it me or jabjab
    """
    return ctx.author.id in [ctx.bot.config.ids.king_id, ctx.bot.owner_id]


class PowerRankingsCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.ranking = {}
        self.negative_words = []
        self.positive_words = []

        self.load_json()

    @commands.group(aliases=['pr'])
    async def powerrankings(self, ctx):
        pass

    @powerrankings.command(aliases=['lb', 'sb', 'leaderboard'])
    async def scoreboard(self, ctx):
        top_ten_ranks = sorted(self.ranking.items(), key=lambda x: x[1], reverse=True)  # [:10]  # not top 10 anymore
        if not top_ten_ranks:
            return
        top_ten_people = []
        maxlen = max(len(str(self.bot.get_user(int(discord_id)))) for discord_id, _ in top_ten_ranks)
        for discord_id, score in top_ten_ranks:
            name = str(self.bot.get_user(int(discord_id)))
            name += ' ' * (maxlen - len(name))
            top_ten_people.append(f'{name}\t{score:+}')
        board = '\n'.join(top_ten_people)
        self.logger.info('%s - %d has requested the powerrankings leaderboard.', ctx.author, ctx.author.id)
        await ctx.send(f'```{board}```')

    @powerrankings.command()
    @commands.check(has_permissions)
    async def remove_negative_words(self, ctx, *words):
        for word in words:
            if word not in self.negative_words:
                await ctx.send(f'`{word}` is not a negative word')
            else:
                self.negative_words.remove(word)
                self.save_json()
                await ctx.send(f'Removed `{word}` from the list of negative words.')

    @powerrankings.command()
    @commands.check(has_permissions)
    async def remove_positive_words(self, ctx, *words):
        for word in words:
            if word not in self.positive_words:
                await ctx.send(f'`{word}` is not a positive word')
            else:
                self.positive_words.remove(word)
                self.save_json()
                await ctx.send(f'Removed `{word}` from the list of positive words.')

    @powerrankings.command()
    @commands.check(has_permissions)
    async def add_negative_words(self, ctx, *words):
        for word in words:
            if word in self.negative_words:
                await ctx.send(f'`{word}` is already a negative word')
            elif word in self.positive_words:
                await ctx.send(f'`{word}` is already a positive word')
            else:
                self.negative_words.append(word)
                self.save_json()
                await ctx.send(f'Added `{word}` to the list of negative words.')

    @powerrankings.command()
    @commands.check(has_permissions)
    async def add_positive_words(self, ctx, *words):
        for word in words:
            if word in self.negative_words:
                await ctx.send(f'`{word}` is already a negative word')
            elif word in self.positive_words:
                await ctx.send(f'`{word}` is already a positive word')
            else:
                self.positive_words.append(word)
                self.save_json()
                await ctx.send(f'Added `{word}` to the list of positive words.')

    @powerrankings.command()
    @commands.check(has_permissions)
    async def list_words(self, ctx):
        pos = '\n'.join(self.positive_words)
        neg = '\n'.join(self.negative_words)
        await ctx.send(f'```Positive words:\n{pos}\n\nNegative words:\n{neg}```')

    async def award_points(self, message, positive):
        """
        Award a random amount of points to the author of the message previous to
        the argument of this message.
        """
        found = False
        tmp_msg = None
        async for msg in message.channel.history(limit=10):
            if found:
                tmp_msg = msg
                break
            if msg.content == message.content and msg.author == message.author:
                found = True

        if not found:
            return

        receiver = str(tmp_msg.author.id)
        if receiver not in self.ranking:
            self.ranking[receiver] = 0
        if positive:
            self.ranking[receiver] += random.randint(1, 3)
        else:
            self.ranking[receiver] -= random.randint(1, 3)
        self.save_json()

    def is_positive(self, message):
        for word in self.positive_words:
            if word in message:
                return True
        return False

    def is_negative(self, message):
        for word in self.negative_words:
            if word in message:
                return True
        return False

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.id == self.bot.user.id:
            return
        if not message.content.strip().startswith('!'):
            if self.is_positive(message.content):
                await self.award_points(message, True)
            if self.is_negative(message.content):
                await self.award_points(message, False)

    def load_json(self):
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning('Couln\'t find %s directory, created new one.', PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, 'r', encoding='utf-8') as fp:
                d = json.load(fp)
                self.ranking = d['ranking']
                self.negative_words = d['negative_words']
                self.positive_words = d['positive_words']
        else:
            self.save_json()
            self.logger.warning('Couldn\'t find %s, created new one.', FILE_PATH)

    def save_json(self):
        d = {
            'ranking': self.ranking,
            'negative_words': self.negative_words,
            'positive_words': self.positive_words
        }
        with open(FILE_PATH, 'w', encoding='utf-8') as fp:
            json.dump(d, fp, indent=2)


async def setup(bot: Bot):
    await bot.add_cog(PowerRankingsCog(bot))
