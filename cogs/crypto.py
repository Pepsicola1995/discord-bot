import logging

from Crypto.Hash import SHA256, SHA512, MD5, MD4, SHA, RIPEMD
from discord.ext import commands

from bot import Bot


class CryptoCog(commands.Cog):
    """Cog to do cryptography (on a grape)"""
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.group()
    async def crypto(self, ctx):
        pass

    @crypto.command(name="hash")
    async def create_hash(self, ctx, algo, *, input):
        """
        Produces a hexdigest of the given input

        Parameters
        ----------
        algo: str
            the hash algorithm you want to use. Available algos are:
            - SHA1
            - SHA256
            - SHA512
            - MD4
            - MD5
            - RIPEMD160

        input: str
            utf-8 encoded string that you want to hash
            strips of any leading or trailing newlines and whitespaces

        Returns
        -------
        hexdigest: str
            ascii representation of the hexdigest of your hash value

        Example
        -------
        >>> !crypto hash sha512 mysupersecurepassword
        f1fddcc4b6b5a0eadb72aba58fb41d3cafbb8f354f44a45c280a810fabfe2d46
        """
        algos = {
            "sha256": SHA256,
            "sha512": SHA512,
            "md5": MD5,
            "md4": MD4,
            "sha1": SHA,
            "sha": SHA,
            "ripemd": RIPEMD,
            "ripemd160": RIPEMD

        }
        algo_id = algo.lower()
        if algo_id not in algos:
            await ctx.send_help(ctx.command)
            return await ctx.send("Invalid algorithm. Valid algorithms are: "
                                  f"{list(algos.keys())}")
        h = algos[algo_id].new()
        h.update(input.strip().encode("utf-8"))
        self.logger.info('%s - %s hashed something with the algo %s',
                         ctx.author,
                         ctx.author.id,
                         algo_id)
        await ctx.send(h.hexdigest())


async def setup(bot: Bot):
    await bot.add_cog(CryptoCog(bot))
