import enum
import json
import logging
import random
import re
import sys
import traceback
from typing import Dict

from discord.ext import commands

from bot import Bot

# reference: https://developer.valvesoftware.com/wiki/SteamID


class InvalidSteamIDError(Exception):
    pass


class CustomIDNotFoundError(Exception):
    pass


class NoCustomIDSetError(Exception):
    pass


class NonExistantID64Error(Exception):
    pass


class SteamID(enum.Enum):
    INVALID = 0
    ID64 = 1
    ID32 = 2
    ID3 = 3
    VANITY = 4


class SteamCog(commands.Cog):
    """
    Cog for steam commands

    Some handy steam id conversion tools and quick
    access to some APIs by any steam id type.
    APIs supported atm:
        - etf2l
        - logs.tf
        - rocketleague.tracker.network
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    # steam utilities:

    @staticmethod
    def check_format(steamid: str) -> SteamID:
        """
        Chek what format a steamid is in

        Parameters
        ----------
        steamid : str
            the steamid you want to check

        Returns
        -------
        SteamID
            Enum variant of the steamID
        """
        id_format = SteamID.INVALID
        if steamid.isdigit():
            maybe_id64 = SteamCog.sanitize_steamid64(steamid)
            if maybe_id64 & 0x110000100000000 == 0x110000100000000:
                id_format = SteamID.ID64
        elif len(re.findall(r"STEAM_0:[0-1]:\d+", steamid)) > 0:
            id_format = SteamID.ID32
        elif len(re.findall(r"\[U:1:\d+\]", steamid)) > 0:
            id_format = SteamID.ID3
        elif steamid.replace("-", "a").replace("_", "a").isalnum():
            id_format = SteamID.VANITY
        return id_format

    @staticmethod
    def get_id_from_steam_link(steamid: str) -> str:
        """
        Get the ID from a steam link (no matter whether it's an ID64 or a vanity ID)

        Parameters
        ----------
        steamid : str
            the steam link you want to get the id of
        """
        return steamid.strip().strip("/").split("/")[-1]

    async def get_steamid64(self, steam_id: str) -> int:
        """
        Return the steamid64 given any format of steamid

        Paramteters
        -----------
        steam_id: str()
            the users steamid

        Returns
        -------
        int
            the users steamid64
        """
        if "steamcommunity.com/" in steam_id:
            steam_id = self.get_id_from_steam_link(steam_id)
        id_format = self.check_format(steam_id)
        if id_format == SteamID.INVALID:
            raise InvalidSteamIDError(steam_id)
        if id_format == SteamID.ID64:
            steamid64 = self.sanitize_steamid64(steam_id)
        elif id_format == SteamID.ID32:
            steamid64 = self.steamid32_to_steamid64(steam_id)
        elif id_format == SteamID.ID3:
            steamid64 = self.steamid3_to_steamid64(steam_id)
        else:
            steamid64 = await self.customid_to_steamid64(steam_id)
        return steamid64

    @staticmethod
    def sanitize_steamid64(steam_id: str) -> int:
        """
        Only the last 32bit identify you, and valve allows some link shenanigans
        """
        return (int(steam_id) & 0xFFFFFFFF) | 0x110000100000000

    # steamid64 to everything:
    @staticmethod
    def steamid64_to_steamid32(steam_id: int) -> str:
        """
        Convert a steamid64 to a steamid32

        Parameters
        ----------
        steamid : int
            the users steamid64


        Returns
        -------
        str
            the users steamid32
        """
        steam_x = steam_id >> 56
        steam_y = steam_id & 1
        steam_z = (steam_id & 0xFFFFFFFF) >> 1
        return f'STEAM_{steam_x}:{steam_y}:{steam_z}'

    @staticmethod
    def steamid64_to_steamid3(steam_id: int) -> str:
        """
        Convert a steamid64 to a steam3id

        Parameters
        ----------
        steamid : int
            the users steamid64

        Returns
        -------
        str
            the users steam3id
        """
        type_to_letter = 'IUMGAPCgT a'  # type as int is the index to this string, e.g. 0 => I
        account_type = (steam_id >> 52) & 0xF
        steam_w = steam_id & 0xFFFFFFFF
        return f'[{type_to_letter[account_type]}:1:{steam_w}]'

    async def steamid64_to_customid(self, steam_id: int) -> str:
        """
        Get the customid for a user

        Parameters
        ----------
        steamid : int
            the users steamid64

        Returns
        -------
        str
            the users customid
        """
        url = ("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/"
               f"v0002/?key={self.bot.config.steam_api_key}&steamids={steam_id}")
        async with self.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
            if len(data["response"]["players"]) == 0:
                raise NonExistantID64Error(steam_id)
            profileurl = data["response"]["players"][0]["profileurl"] \
                .split("/")[-2]
            if str(profileurl) != str(steam_id):
                return profileurl
            raise NoCustomIDSetError(steam_id)

    async def steamid64_to_current_nickname(self, steam_id: int) -> str:
        """
        Get the current nickame of a user

        Parameters
        ----------

        steamid : int
            the users steamid64

        Returns
        -------
        str
            the users current nickname
        -1
            no player found
        """
        url = ("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/"
               f"v0002/?key={self.bot.config.steam_api_key}&steamids={steam_id}")
        async with self.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
            if len(data["response"]["players"]) == 0:
                raise NonExistantID64Error(steam_id)
            nickname = data["response"]["players"][0]["personaname"]
            return nickname

    # everything to steamid64:
    @staticmethod
    def steamid32_to_steamid64(steamid: str) -> int:
        """
        Convert a steamid32 to a steamid64

        Parameters
        ----------
        steamid : str
            the users steamid32

        Returns
        -------
        int
            the users steamid64
        """
        steam_y, steam_z = map(int, steamid.strip(' STEAM_').split(':')[1:])
        steam_64 = (steam_z << 1) | steam_y | 0x110000100000000
        return steam_64

    @staticmethod
    def steamid3_to_steamid64(steamid: str) -> int:
        """
        Convert a steam3id to a steamid64

        Parameters
        ----------
        steamid : str
            the users steam3id

        Returns
        -------
        int
            the users steamid64
        """
        steam32 = int(steamid.strip().split(':')[-1][:-1])
        steam64 = steam32 | 0x110000100000000
        return steam64

    async def customid_to_steamid64(self, steamid: str) -> int:
        """
        Convert a customid to a steamid64

        Parameters
        ----------
        steamid : str
            the users customid

        Returns
        -------
        int
            the users steamid64
        """
        url = ("http://api.steampowered.com/ISteamUser/ResolveVanityURL/"
               f"v0001/?key={self.bot.config.steam_api_key}&vanityurl={steamid}")
        async with self.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
        if data["response"]["success"] == 1:
            steamid64 = data["response"]["steamid"]
        else:
            raise CustomIDNotFoundError(steamid)
        return int(steamid64)

    async def player_summary(self, steamid64: int) -> Dict:
        """
        Query the steam API for the player summary of a player
        """
        url = ("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/"
               f"?key={self.bot.config.steam_api_key}&steamids={steamid64}")
        async with self.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
        player = data['response']["players"][0]
        return player

    @commands.group()
    async def steam(self, ctx):
        """
        Group for steam commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @steam.command()
    async def userinfo(self, ctx: commands.Context, steam_id: str):
        """
        Show a message with lots of useful info about the user.

        Parameters
        ----------
        steam_id : str
            any steam_id of the user you want to look up

        Example
        -------
        >>> !steam userinfo St4ck
        Gives you info about the steam user with the id "St4ck"
        """
        steam_id64 = await self.get_steamid64(steam_id)
        steam_id32 = self.steamid64_to_steamid32(steam_id64)
        steam3id = self.steamid64_to_steamid3(steam_id64)
        try:
            custom_id = await self.steamid64_to_customid(steam_id64)
        except NoCustomIDSetError:
            custom_id = "No custom id set."
        nickname = await self.steamid64_to_current_nickname(steam_id64)
        await ctx.send(f"```|   nickname: {nickname}\n"
                       f"|  steamid64: {steam_id64}\n"
                       f"|  steamid32: {steam_id32}\n"
                       f"|   steam3id: {steam3id}\n"
                       "| profileurl: https://steamcommunity.com/profiles/"
                       f"{steam_id64}\n"
                       f"|   customID: {custom_id}```")
        self.logger.info('%s - %s looked up a steam user with the ID %s',
                         ctx.author,
                         ctx.author.id,
                         steam_id64)

    @steam.command()
    async def link(self, ctx: commands.Context, steam_id: str):
        """
        Show a link to the users profiles

        Parameters
        ----------
        steam_id : str
            any steam id of the user you want to get the link for

        Example
        -------
        >>> !steam link STEAM_0:1:31574593
        Gives you a link to the steam user with the id "STEAM_0:1:31574593"
        """
        steam_id64 = await self.get_steamid64(steam_id)
        await ctx.send(f"https://steamcommunity.com/profiles/{steam_id64}")
        self.logger.info('%s - %s searched for the link of user %s',
                         ctx.author,
                         ctx.author.id,
                         steam_id64)

    @steam.command()
    async def snipe(self, ctx: commands.Context, steam_id: str):
        """
        Get a join link to the server the player is currently playing on
        """
        steam_id64 = await self.get_steamid64(steam_id)
        player_data = await self.player_summary(steam_id64)
        if "gameserverip" not in player_data:
            return await ctx.send("User is currently not in game "
                                  "or has their server IP private.")
        return await ctx.send(f"steam://connect/{player_data['gameserverip']}")

    @steam.command()
    async def logs(self, ctx: commands.Context, steam_id: str):
        """
        Show a link to the users logs.tf profile

        Parameters
        ----------
        steam_id : str
            any steam id of the user you want to get the logs.tf profile for

        Example
        -------
        >>> !steam logs [U:1:63149187]
        Gives you a link to the logs.tf profile of the steam user with the id
        [U:1:63149187]
        """
        steam_id64 = await self.get_steamid64(steam_id)
        await ctx.send(f"https://logs.tf/profile/{steam_id64}")
        self.logger.info('%s - %s searched for the logs of user with the ID %s',
                         ctx.author,
                         ctx.author.id,
                         steam_id64)

    @steam.command()
    async def etf2l(self, ctx: commands.Context, steam_id: str):
        """
        Return a link to that users etf2l profile
        Parameters
        ----------
        steam_id : str
            any steam id of the user you want to get the etf2l profile for

        Example
        -------
        >>> !steam etf2l St4ck
        Sends you a link to the etf2l profile for St4ck
        """
        steam_id64 = await self.get_steamid64(steam_id)

        url = f"http://api.etf2l.org/player/{steam_id64}.json"
        async with self.bot.aiohttp_session.get(url) as resp:
            if resp.status != 502:
                data = json.loads(await resp.text())
                if data["status"]["code"] == 200:
                    player_id = data["player"]["id"]
                    await ctx.send(f"http://etf2l.org/forum/user/{player_id}")
                    self.logger.info('%s - %s looked up the etf2l profile for ID %s',
                                     ctx.author,
                                     ctx.author.id,
                                     steam_id64)
                else:
                    response = data["status"]["message"]
                    await ctx.send(f"etf2l error: {response}")
            else:
                await ctx.send('Seems like the etf2l API is currently unreachable.')

    @steam.command(name="add")
    async def steam_add(self, ctx: commands.Context, steam_id: str):
        """
        Show a steam add link for the given steam id

        Parameters
        ----------
        steam_id : str
            the steam id of the user you want to add

        Example
        -------
        >>> !steam add St4ck
        Shows you a link to directly add the steam user with the id "St4ck"
        """
        steam_id64 = await self.get_steamid64(steam_id)
        await ctx.send(f"steam://friends/add/{steam_id64}")
        self.logger.info('%s - %s looked up a steam friend link with the ID %s',
                         ctx.author,
                         ctx.author.id,
                         steam_id64)

    @steam.command(aliases=['integrity', 'integrity_check', 'verify'])
    async def check_integrity(self, ctx: commands.Context, steam_id64: str):
        """
        Check whether the steamid64 you have has been fiddled with,
        even if it links to the correct account.

        Parameters
        ----------
        steam_id64 : str
            the steamid64 you want to check

        Example
        -------
        >>> !steam check_integrity 76561198023414915
        This id is good :thumbsup:
        """
        id_type = self.check_format(steam_id64)
        if id_type != SteamID.ID64:
            await ctx.send_help(ctx.command)
            return await ctx.send('Invalid ID format! I need an ID64.')
        real_id64 = self.sanitize_steamid64(steam_id64)
        self.logger.info('%s - %s checked the integrity of the ID %s',
                         ctx.author, ctx.author.id, steam_id64)

        if steam_id64 != real_id64:
            return await ctx.send(f':no_entry_sign: The ID `{steam_id64}` is '
                                  f'**NOT** valid. The real ID is '
                                  f'`{real_id64}` :no_entry_sign:')
        return await ctx.send(f'The ID `{steam_id64}` is valid. :white_check_mark:')

    # additional bits may at most be 1900 decimal places, so at most math.log2(10 ** 1900) ~= 6311
    @steam.command(aliases=['randomize', 'randomise', 'randomise_link'], hidden=True)
    async def randomize_link(self, ctx: commands.Context, steamid: str, additional_bits: commands.Range[int, 0, 6311] = 0):
        """
        Gives you a random link to your profile

        Parameters
        ----------
        steamid : str
            the steamid you want a linkto
        additional_bits : int
            The number of bits you want to add to the usual 64bit steam ID

        Example
        -------
        >>> !steam randomize_link St4ck
        https://steamcommunity.com/profiles/78486846610576515
        """
        steam_id64 = await self.get_steamid64(steamid)
        important = int(steam_id64) & 0xFFF00000FFFFFFFF
        middle_bits = random.randrange(0x100000) << 32
        outer_bits = random.randrange(2**additional_bits) << 64
        extended_id = important | middle_bits | outer_bits
        link = f"https://steamcommunity.com/profiles/{extended_id}"
        await ctx.send(link)
        self.logger.info('%s - %s randomized a steam link with the ID %s',
                         ctx.author,
                         ctx.author.id,
                         steam_id64)

    @commands.command(help=steam_add.help, hidden=True)
    async def add(self, ctx, steam_id=None):
        await ctx.invoke(self.steam_add, steam_id=steam_id)

    @userinfo.error
    @link.error
    @snipe.error
    @logs.error
    @etf2l.error
    @steam_add.error
    @check_integrity.error
    @randomize_link.error
    async def error_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send_help(ctx.command)
            return await ctx.send('You need to specify the steamID')
        if isinstance(error, InvalidSteamIDError):
            await ctx.send_help(ctx.command)
            return await ctx.send(f'I do not recognize the steamID format of `{error}`')
        if isinstance(error, CustomIDNotFoundError):
            return await ctx.send(f'Could not find anyone with the vanityID `{error}`')
        if isinstance(error, NonExistantID64Error):
            return await ctx.send(f'Could not find a player with the steamID64 `{error}`')
        if isinstance(error, commands.BadArgument):
            if isinstance(error, commands.RangeError):
                return await ctx.send(f'Argument out of range: {error}')
            return await ctx.send(f'Bad argument: {error}')
        print(f'Ignoring exception in command {ctx.command}:', file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


async def setup(bot: Bot):
    await bot.add_cog(SteamCog(bot))
