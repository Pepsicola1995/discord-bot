from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import Boolean
from sqlalchemy import MetaData
from sqlalchemy import Text
from sqlalchemy.orm import declarative_base


Base = declarative_base(metadata=MetaData(schema='discord_streams'))


class Subscription(Base):
    __tablename__ = 'subscriptions'
    streamer = Column('streamer', BigInteger, primary_key=True)
    subscriber = Column('subscriber', BigInteger, primary_key=True)

    def __repr__(self):
        return f'<Subscription subscriber={self.subscriber}, streamer={self.streamer}>'


class Streamer(Base):
    __tablename__ = 'streamers'
    id = Column('streamer', BigInteger, primary_key=True)
    status = Column('status', Text)
    last_status_update = Column('last_status_update', BigInteger)
    is_live = Column('is_live', Boolean)
    last_seen = Column('last_seen', BigInteger)

    def __repr__(self):
        return (f'<Streamer id={self.id}, status={self.status}, '
                f'last_status_update={self.last_status_update}, is_lve={self.is_live}, '
                f'last_seen={self.last_seen}>')


class Following(Base):
    __tablename__ = 'followings'
    game = Column('game', Text, primary_key=True)
    follower = Column('follower', BigInteger, primary_key=True)

    def __repr__(self):
        return f'<Following game={self.game}, follower={self.follower}>'


