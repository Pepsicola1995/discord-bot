import enum
from typing import Optional

import discord
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import BigInteger
from sqlalchemy import Boolean
from sqlalchemy import ForeignKey
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base
from sqlalchemy.schema import Identity

from config import config

Base = declarative_base(metadata=MetaData(schema='jabomons'))


class Rarity(enum.IntEnum):
    COMMON = 1
    UNCOMMON = 2
    RARE = 3
    EPIC = 4
    LEGENDARY = 5


class Jabkscore(enum.Enum):
    MIN = config.ids.emt_min
    TWO = config.ids.emt_two
    THREE = config.ids.emt_three
    FOUR = config.ids.emt_four
    FIVE = config.ids.emt_five
    SIX = config.ids.emt_six
    SEVEN = config.ids.emt_seven
    EIGHT = config.ids.emt_eight
    NINE = config.ids.emt_nine
    TEN = config.ids.emt_ten
    MAX = config.ids.emt_max


class RLRank(enum.IntEnum):
    Bronze_I = 0
    Bronze_II = 1
    Bronze_III = 2
    Silver_I = 3
    Silver_II = 4
    Silver_III = 5
    Gold_I = 6
    Gold_II = 7
    Gold_III = 8
    Platinum_I = 9
    Platinum_II = 10
    Platinum_III = 11
    Diamond_I = 12
    Diamond_II = 13
    Diamond_III = 14
    Champion_I = 15
    Champion_II = 16
    Champion_III = 17
    Grand_Champion_I = 18
    Grand_Champion_II = 19
    Grand_Champion_III = 20
    Super_Sonic_Legend = 21


class Quality(enum.IntEnum):
    Battle_Scarred = 0
    Well_Worn = 1
    Field_Tested = 2
    Minimal_Wear = 3
    Factory_New = 4


class EventType(enum.Enum):
    ROLL = 1
    GUESS = 2
    EVOLUTION = 3
    COMBINATION = 4


class Jabomon(Base):
    __tablename__ = 'jabomons'
    id = Column('id', BigInteger, Identity(), primary_key=True)
    rarity = Column('rarity', Enum(Rarity))


class Item(Base):
    __tablename__ = 'items'
    id = Column('id', BigInteger, Identity(), primary_key=True)
    inventory_id = Column('inventory_id', BigInteger)
    jabomon_id = Column('jabomon_id', BigInteger, ForeignKey(Jabomon.id, ondelete='CASCADE'))
    shiny = Column('shiny', Boolean)
    jabkscore = Column('jabkscore', Enum(Jabkscore))
    quality = Column('quality', Enum(Quality))
    rl_rank = Column('rl_rank', Enum(RLRank))

    def to_member(self, guild: discord.Guild) -> Optional[discord.Member]:
        return guild.get_member(self.id)


class User(Base):
    __tablename__ = 'users'
    id = Column('id', BigInteger, primary_key=True)


class Event(Base):
    __tablename__ = 'events'
    id = Column('id', BigInteger, Identity(), primary_key=True)
    timestamp = Column('timestamp', DateTime)
    type = Column('type', Enum(EventType))
    user_id = Column('user_id', BigInteger, ForeignKey(User.id, ondelete='CASCADE'))
    jabomon_id = Column('jabomon_id', BigInteger, ForeignKey(User.id, ondelete='CASCADE'))
    shiny = Column('shiny', Boolean)
    jabkscore = Column('jabkscore', Enum(Jabkscore))
    quality = Column('quality', Enum(Quality))
    rl_rank = Column('rl_rank', Enum(RLRank))
