from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import Integer
from sqlalchemy import MetaData
from sqlalchemy import DateTime
from sqlalchemy.orm import declarative_base

Base = declarative_base(metadata=MetaData(schema='emoji_powerrankings'))

class Record(Base):
    __tablename__ = 'records'
    member_id = Column('member_id', BigInteger, primary_key=True)
    no_messages = Column('no_messages', Integer)
    no_words = Column('no_words', BigInteger)
    no_emojis = Column('no_emojis', BigInteger)
    last_message_date = Column('last_message_date', DateTime)

    def __repr__(self) -> str:
        return (f'<EmojiPowerrankingRecord member_id={self.member_id}, '
                f'no_messages={self.no_message}, '
                f'no_words={self.no_words}, '
                f'no_emojis={self.no_emojis}, '
                f'last_message_date={self.last_message_date}>')


class CachedRecord(Base):
    __tablename__ = 'cached_records'
    member_id = Column('member_id', BigInteger, primary_key=True)
    no_messages = Column('no_messages', Integer)
    no_words = Column('no_words', BigInteger)
    no_emojis = Column('no_emojis', BigInteger)
    last_message_date = Column('last_message_date', DateTime)

    def __repr__(self) -> str:
        return (f'<EmojiPowerrankingRecord member_id={self.member_id}, '
                f'no_messages={self.no_message}, '
                f'no_words={self.no_words}, '
                f'no_emojis={self.no_emojis}, '
                f'last_message_date={self.last_message_date}>')


class CacheTimestamp(Base):
    __tablename__ = 'cache_timestamps'
    timestamp = Column('timestamp', DateTime, primary_key=True)
