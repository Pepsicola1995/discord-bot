from typing import Callable

from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker as _sessionmaker

from config import config

ASYNC_ENGINE = create_async_engine(
    f'postgresql+asyncpg://{config.pg_user}:{config.pg_pass}@{config.pg_host}:5432/discordbot'
)
BLOCKING_ENGINE = create_engine(
    f'postgresql://{config.pg_user}:{config.pg_pass}@{config.pg_host}:5432/discordbot'
)
async_sessionmaker: Callable[[], AsyncSession] = _sessionmaker(ASYNC_ENGINE, expire_on_commit=False, class_=AsyncSession)  # type: ignore
