from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import MetaData
from sqlalchemy import BigInteger
from sqlalchemy import DateTime
from sqlalchemy.orm import declarative_base
from sqlalchemy.schema import Identity

Base = declarative_base(metadata=MetaData(schema='testing'))


class Roll(Base):
    __tablename__ = 'rolls'
    id = Column('id', BigInteger, Identity(), primary_key=True)
    timestamp = Column('timestamp', DateTime)
    roller_id = Column('roller_id', BigInteger)
    rolled_id = Column('rolled_id', BigInteger)


class CountClick(Base):
    __tablename__ = 'count_clicks'
    discord_id = Column('discord_id', BigInteger, primary_key=True)
    amount = Column('amount', Integer)


class CountMeta(Base):
    __tablename__ = 'count_meta'
    count = Column('count', BigInteger, primary_key=True)
    last_click_id = Column('last_click_id', BigInteger, primary_key=True)
