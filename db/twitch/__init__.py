from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import Boolean
from sqlalchemy import Text
from sqlalchemy import ForeignKey
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base(metadata=MetaData(schema='twitch'))


class Streamer(Base):
    __tablename__ = 'streamers'
    name = Column('name', Text, primary_key=True)
    is_live = Column('is_live', Boolean)
    subscriptions = relationship('Subscription', cascade='all, delete', passive_deletes=True)


class Subscription(Base):
    __tablename__ = 'subscriptions'
    streamer = Column('streamer', Text, ForeignKey(Streamer.name, ondelete='CASCADE'), primary_key=True)
    subscriber = Column('subscriber', BigInteger, primary_key=True)


class OauthToken(Base):
    __tablename__ = 'oauth_token'
    token = Column('token', Text, primary_key=True)

