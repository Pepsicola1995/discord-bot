from sqlalchemy import BigInteger
from sqlalchemy import Integer
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column

Base = declarative_base(metadata=MetaData(schema='activity'))


class Record(Base):
    __tablename__ = 'activity_records'
    member_id = Column('member_id', BigInteger, primary_key=True)
    channel_id = Column('channel_id', BigInteger, primary_key=True)
    message_count = Column('message_count', Integer, default=0)
    word_count = Column('word_count', Integer, default=0)
    message_count_monthly = Column('message_count_monthly', Integer, default=0)
    word_count_monthly = Column('word_count_monthly', Integer, default=0)

    def __repr__(self):
        return (f'<ActivityRecord member_id={self.member_id}, '
                f'channel_id={self.channel_id}, '
                f'message_count={self.message_count}, '
                f'word_count={self.word_count}, '
                f'message_count_monthly={self.message_count_monthly}, '
                f'word_count_monthly={self.word_count_monthly}>')

    def __str__(self):
        return self.__repr__()


class Month(Base):
    __tablename__ = 'months'
    number = Column('number', Integer, primary_key=True, default=0)


class Normie(Base):
    """A user that will not get tracked"""
    __tablename__ = 'normies'
    member_id = Column('member_id', BigInteger, primary_key=True)
