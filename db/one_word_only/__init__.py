from __future__ import annotations

import datetime

import discord
from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base

Base = declarative_base(metadata=MetaData(schema='one_word_only'))

class Message(Base):
    __tablename__ = 'messages'
    id = Column('id', BigInteger, primary_key=True)
    timestamp = Column('timestamp', DateTime)
    content = Column('content', Text)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f'<Message id={self.id}, timestamp={self.timestamp}, content="{self.content}">'

    @classmethod
    def from_discord_message(cls, message: discord.Message) -> Message:
        naive_dt = message.created_at.replace(tzinfo=None)
        return cls(id=message.id, timestamp=naive_dt, content=message.content)

    def get_utc_time(self):
        return self.timestamp.replace(tzinfo=datetime.timezone.utc)

class Day(Base):
    __tablename__ = 'days'
    day = Column('day', Integer, primary_key=True)
