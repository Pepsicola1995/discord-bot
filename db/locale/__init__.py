import datetime

import dateutil.tz
from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import Text
from sqlalchemy.orm import declarative_base


Base = declarative_base()


class Locale(Base):
    __tablename__ = 'locales'
    id = Column('id', BigInteger, primary_key=True)
    locale = Column('locale', Text)

    def __str__(self) -> str:
        return str(self.locale)

    @property
    def tzinfo(self) -> datetime.tzinfo:
        return dateutil.tz.gettz(self.locale)
