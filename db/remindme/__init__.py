import datetime

from sqlalchemy import Column
from sqlalchemy import BigInteger
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy import DateTime
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base

Base = declarative_base(metadata=MetaData(schema='remindme'))


class Reminder(Base):
    __tablename__ = 'reminders'
    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger)
    text = Column(Text)
    time = Column(DateTime)

    def __repr__(self) -> str:
        return (f'<Reminder id={self.id}, user_id={self.user_id}, text={self.text}, '
                f'time={self.time}>')

    @property
    def utc_time(self) -> datetime.datetime:
        return self.time.replace(tzinfo=datetime.timezone.utc)

    def localtime(self, tz: datetime.tzinfo) -> datetime.datetime:
        return self.utc_time.astimezone(tz)


class Separator(Base):
    __tablename__ = 'separators'
    user_id = Column(BigInteger, primary_key=True)
    separator = Column(Text, primary_key=True)
