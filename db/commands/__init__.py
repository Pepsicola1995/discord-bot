from sqlalchemy import Column
from sqlalchemy import MetaData
from sqlalchemy import Text
from sqlalchemy.orm import declarative_base

Base = declarative_base(metadata=MetaData(schema='commands'))


class Command(Base):
    __tablename__ = 'commands'
    name = Column('name', Text, primary_key=True)
    content = Column('content', Text)

    def __repr__(self):
        return f'<Command name={self.name}, content={self.content}>'

