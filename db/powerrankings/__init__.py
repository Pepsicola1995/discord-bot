from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Integer
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base


Base = declarative_base(metadata=MetaData(schema='powerrankings'))


class Record(Base):
    __tablename__ = 'records'
    member_id = Column('member_id', BigInteger, primary_key=True)
    day = Column('day', Date, primary_key=True)
    no_messages = Column('no_messages', Integer)
    no_words = Column('no_words', Integer)
    no_emojis = Column('no_emojis', Integer)

    def __repr__(self) -> str:
        return (f'<PowerrankingRecord member_id={self.member_id}, '
                f'day={self.day.isoformat()}'
                f'no_messages={self.no_messages}, '
                f'no_words={self.no_words}, '
                f'no_emojis={self.no_emojis}')
