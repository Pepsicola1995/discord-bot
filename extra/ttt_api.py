import json
import urllib.parse

from bottle import get, run, response, request
from transformers import pipeline

TEXT_GENERATOR = pipeline('text-generation')
SENTIMENT_ANALYZER = pipeline('sentiment-analysis')

@get('/sentiment')
def sentiment():
    response.content_type = 'application/json'
    response.status = 200
    inp_text = urllib.parse.unquote_plus(request.params['input'])
    result = SENTIMENT_ANALYZER([inp_text])
    return json.dumps(result)

@get('/complete')
def complete():
    response.content_type = 'application/json'
    response.status = 200
    inp_text = urllib.parse.unquote_plus(request.params['input'])
    max_length = int(request.params.get('max_length', '150'))
    generated_text = TEXT_GENERATOR(inp_text, max_length=max_length, do_sample=True)[0]['generated_text']
    return json.dumps({'result': urllib.parse.quote_plus(generated_text)})


run(host='127.0.0.1', port=61234)
