from __future__ import annotations

import json
from dataclasses import dataclass

@dataclass
class IDs:
    # guilds
    guild_id: int
    # categories
    gaming_category: int
    # channels and threads
    bot_channel_id: int
    stream_channel_id: int
    tavern_id: int
    jousting_stadium: int
    tournament_suggestions: int
    one_word_only_channel: int
    reaction_roles_channel: int
    # roles
    knight_role_id: int
    council_role: int
    state_secretary_role: int
    admin_role: int
    # users
    no_command_user_id: int
    king_id: int
    # emotes
    emt_min: int
    emt_two: int
    emt_three: int
    emt_four: int
    emt_five: int
    emt_six: int
    emt_seven: int
    emt_eight: int
    emt_nine: int
    emt_ten: int
    emt_max: int

@dataclass
class Config:
    ids: IDs
    token: str
    catapi_key: str
    challonge_api_key: str
    steam_api_key: str
    twitch_id: str
    twitch_secret: str
    trn_api_key: str
    rlstats_api_key: str
    vps_ip: str
    vps_domain: str
    twitter_consumer_key: str
    twitter_consumer_secret: str
    twitter_access_token_key: str
    twitter_access_token_secret: str
    pg_user: str
    pg_pass: str
    pg_host: str
    rarities: dict[int, float]
    report_reasons: list[dict[str, str]]
    shiny_chance: float

    @classmethod
    def from_json(cls, data: dict) -> Config:
        data['ids'] = IDs(**data['ids'])
        data['rarities'] = {int(k): v for k, v in data['rarities'].items()}  # make rarity keys ints
        return cls(**data)


with open('./config.json', 'r', encoding='utf-8') as filp:
    config = Config.from_json(json.load(filp))
